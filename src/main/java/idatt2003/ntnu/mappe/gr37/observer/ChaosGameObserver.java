package idatt2003.ntnu.mappe.gr37.observer;

/**
 * This interface is an observer for the ChaosGame class.
 * It is used to update the GUI when the canvas changes.
 * It is implemented by the ChaosCanvasObserver class.
 */

public interface ChaosGameObserver {

  void update();
}
