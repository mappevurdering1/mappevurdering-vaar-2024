package idatt2003.ntnu.mappe.gr37.observer;

import idatt2003.ntnu.mappe.gr37.button.mainTab.CanvasView;
import idatt2003.ntnu.mappe.gr37.game.ChaosCanvas;

/**
 * This class is an observer for the ChaosCanvas class.
 * It is used to update the canvas when the description changes.
 * It implements the ChaosGameObserver interface.
 *
 * @see ChaosGameObserver
 */

public class ChaosCanvasObserver implements ChaosGameObserver {

  /**
   * The {@code Canvas} that is being observed.
   */

  private ChaosCanvas canvas;

  /**
   * Constructor for the ChaosCanvasObserver class. It take in the {@code ChaosCanvas} as a
   * parameter and initializes the {@code Canvas} to display the result to the user.
   *
   * @param canvas the canvas to observe.
   */

  public ChaosCanvasObserver(ChaosCanvas canvas) {
    this.canvas = canvas;
  }

  /**
   * Updates the canvas.
   */

  @Override
  public void update() {
    CanvasView.drawCanvas(canvas.getCanvasArray());
  }
}