package idatt2003.ntnu.mappe.gr37.game;

import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import idatt2003.ntnu.mappe.gr37.observer.ChaosCanvasObserver;
import idatt2003.ntnu.mappe.gr37.observer.ChaosGameObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The ChaosGame class represents the chaos game itself. It initializes the canvas to draw the game,
 * the description of the game and the current point. The Chaos game is a game where a random
 * transformation is chosen from a list, then the current point is transformed to the canvas and
 * drawn. This process is repeated a set amount of times with the runSteps() method to create a
 * fractal. Which is a repeatable pattern that is self-similar wherever you want to zoom in on it,
 * or out of it.
 */

public class ChaosGame {
  private final ChaosGameDescription description;
  private final ChaosCanvas canvas;
  private Vector2D currentPoint;
  public Random random;
  private List<ChaosGameObserver> observers;

  /**
   * Constructor for the ChaosGame class. It initializes the ChaosGameDescription object, the
   * ChaosCanvas object with its width and height, and the current point as a Vector2D object.
   *
   * @param description ChaosGameDescription object with the list of transformations, the min and
   *                    max coordinates of the canvas.
   * @param width the width of the canvas.
   * @param height the height of the canvas.
   */

  public ChaosGame(ChaosGameDescription description, int width, int height) {
    this.description = description;
    this.currentPoint = new Vector2D(0.0, 0.0);
    this.random = new Random();
    this.observers = new ArrayList<>();
    Vector2D minCoordinate = description.getMinCoords();
    Vector2D maxCoordinate = description.getMaxCoords();
    this.canvas = new ChaosCanvas(width, height, minCoordinate, maxCoordinate);
    // canvas observer
    ChaosCanvasObserver canvasObserver = new ChaosCanvasObserver(canvas);
    attachObserver(canvasObserver);
  }

  /**
   * Runs the chaos game with a set amount of steps. A random number within the list of
   * transformations is chosen, then that transformation is applied to the current point, and the
   * point is drawn on the canvas with the putPixel() method.
   *
   * @param steps the amount of steps the game will run the chaos game.
   */

  public void runSteps(int steps) {
    for (int i = 0; i < steps; i++) {
      int chooseTransformation = random.nextInt(0, this.description.getTransforms().size());
      currentPoint =
        this.description.getTransforms().get(chooseTransformation).transform(currentPoint);
      canvas.putPixel(currentPoint);
    }
    notifyObservers();
  }

  /**
   * Auto generated getter method with IntelliJ. A getter method for the canvas object.
   *
   * @return the canvas object.
   */

  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * This method will add a {@code ChaosGameObserver} to the {@code ArrayList} 'observers'.
   *
   * @param observer Observer object to add.
   */

  public void attachObserver(ChaosGameObserver observer) {
    observers.add(observer);
  }

  /**
   * This method will run the update() - method on all {@code ChaosGameObserver} in the
   * {@code ArrayList} 'observers'.
   */

  private void notifyObservers() {
    observers.forEach(ChaosGameObserver::update);
  }

}
