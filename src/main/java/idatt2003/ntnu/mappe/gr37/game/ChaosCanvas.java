package idatt2003.ntnu.mappe.gr37.game;

import idatt2003.ntnu.mappe.gr37.model.AffineTransform2D;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;

/**
 * ChaosCanvas is a class that represents a canvas for the chaos game. The canvas is a 2D array of
 * integers, where each integer represents a pixel. The canvas is initialized with a width and a
 * height, and a transformation from coordinates to indices on the canvas. The canvas is used to
 * visualise result of the chaos game.
 */

public class ChaosCanvas {
  private int [][] canvas;
  private final int width;
  private final int height;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * Constructor that initializes the canvas and the transformation from coordinates to indices on
   * the canvas.
   *
   * @param width width of the canvas, count of columns (N).
   * @param height height of the canvas, count of rows (M).
   * @param minCoords minimum coordinates, left bottom corner.
   * @param maxCoords maximum coordinates, upper right corner.
   */

  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[width][height];
    this.transformCoordsToIndices = makeCanvasTransformation();
  }

  /**
   * Added this method to simplify constructor, and to make sure the transformations are not getting
   * mixed with the canvas transformation. It is a private method that creates the transformation
   * from coordinates to indices on the canvas.
   */

  private AffineTransform2D makeCanvasTransformation() {
    double a00 = 0.0;
    double a01 = (height - 1) / (minCoords.getX1() - maxCoords.getX1()); // 50 - 1 / 0 - 1 = -49.0
    double a10 = (width - 1) / (maxCoords.getX0() - minCoords.getX0()); // 50 - 1 / 1 - 0 = 49.0
    double a11 = 0.0;
    double x0 = ((height - 1) * maxCoords.getX1()) / ((maxCoords.getX1()) - minCoords.getX1());
    double x1 = ((width - 1) * minCoords.getX0()) / ((minCoords.getX0()) - maxCoords.getX0());
    return new AffineTransform2D(new Matrix2x2(a00, a01, a10, a11), new Vector2D(x0, x1));
  }

  /**
   * Gets the value of a specific pixel on the canvas. A blank pixel is represented by a 0, and a
   * colored pixel is represented by 1.
   *
   * @param point the (x0, x1) coordinates of the pixel.
   * @return either 0 or 1.
   */

  public int getPixel(Vector2D point) {
    // transforms the vector point to indexes in the canvas
    Vector2D indices = transformCoordsToIndices.transform(point);
    int xindex = (int) Math.round(indices.getX0());
    int yindex = (int) Math.round(indices.getX1());

    return canvas[xindex][yindex];
  }

  /**
   * Puts a specified pixel on the canvas. Transforms the Affine transformation to indices on the
   * canvas on the point.
   *
   * @param point {@code Vector2D} point to put on the canvas.
   * @throws IllegalArgumentException if the indices are outside the canvas.
   */

  public void putPixel(Vector2D point) throws IllegalArgumentException {
    Vector2D indices = transformCoordsToIndices.transform(point);
    try {

      if (verifyIndex((int) indices.getX0(), (int) indices.getX1())) {
        int i = (int) indices.getX0();
        int j = (int) indices.getX1();
        canvas[width - i - 1][j] = 1;
      }
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  /**
   * Accesses an array in the canvas.
   *
   * @return canvas array
   */

  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Verifies that the indices are within the canvas arrays. The parameters must be converted from
   * coordinates to indices on the canvas first. The method throws and exception if the indices are
   * outside the canvas, or the value of the canvas if it is inside the canvas. The value can be 0,
   * even if the pixel is inside the canvas.
   *
   * @param xindex x-index on the canvas. Top left to top right.
   * @param yindex y-index on the canvas. Top left to bottom left.
   * @return the value the canvas holds on that coordinate.
   * @throws IllegalArgumentException if the indices are outside the canvas.
   */

  private boolean verifyIndex(int xindex, int yindex) throws IllegalArgumentException {
    return (xindex >= 0 && xindex < width) && (yindex >= 0 && yindex < height);
  }
}
