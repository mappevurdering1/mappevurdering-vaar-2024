package idatt2003.ntnu.mappe.gr37.game;

import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.util.List;

/**
 * This class represents the description of a chaos game. It contains the minimum and maximum
 * coordinates of the game, as well as a list of the transformations. The ChaosGameDescription is
 * used to create a ChaosGame object.
 */

public class ChaosGameDescription {
  /**
   * min coords are related to the bottom left of the game. x0-min, x1-min.
   */
  private final Vector2D minCoords;
  /**
   * max coords are related to the top right of the game. x0-max, x1-max.
   */
  private final Vector2D maxCoords;
  /**
   * represents the list of transformations, either Affine- or julia transformations.
   */
  private final List<Transform2D> transforms;

  /**
   * Constructor for the ChaosGameDescription class.
   *
   * @param minCoords  minimum coordinates as a Vector2D object.
   * @param maxCoords  maximum coordinates as a Vector2D object.
   * @param transforms list of transformations.
   */

  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords,
                              List<Transform2D> transforms) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  /**
   * Auto generated getter method with IntelliJ.
   *
   * @return list of transforms.
   */

  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Auto generated getter method with IntelliJ.
   *
   * @return minimum coordinates as a Vector 2D object.
   */

  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Auto generated getter method with IntelliJ.
   *
   * @return max coordinates as a Vector2D object.
   */

  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Auto generated toString method with IntelliJ. This method is used in the {@code ChaosGame}
   * class to {@code ChaosGameFileHandler} class to write the description to a file.
   * @return a {@code String} representation of the ChaosGameDescription object.
   */

  @Override
  public String toString() {
    return minCoords.getX0() + ", " + minCoords.getX1() + "\n"
        + maxCoords.getX0() + ", " +  maxCoords.getX1();
  }
}
