package idatt2003.ntnu.mappe.gr37.game;

import idatt2003.ntnu.mappe.gr37.model.AffineTransform2D;
import idatt2003.ntnu.mappe.gr37.model.Complex;
import idatt2003.ntnu.mappe.gr37.model.JuliaTransform;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * This class is responsible for reading and writing the description of a chaos game to a file.
 * It reads the description of the chaos game from a file and creates a {@code ChaosGameDescription}
 * object. It also writes the description of the chaos game to a specified file. The class extends
 * {@code ChaosGameDescription}.
 */

public class ChaosGameFileHandler extends ChaosGameDescription {

  /**
   * The constructor initializes the minimum coordinates, maximum coordinates, and the list of
   * transformations.
   *
   * @param minCoords The minimum coordinates of the game.
   * @param maxCoords The maximum coordinates of the game.
   * @param transforms The list of transformations.
   */

  public ChaosGameFileHandler(Vector2D minCoords, Vector2D maxCoords,
                              List<Transform2D> transforms) {
    super(minCoords, maxCoords, transforms);
  }

  /**
   * Method was made with help from ChatGPT. ChatGPT was used to explain the task in detail, and
   * with implementation of the BufferedReader and FileReader.
   * This method reads the lines of a file if the reader finds; "#" it will skip reading them.
   * The method then reads the lines and implements the result of each line into a new
   * {@code ChaosGameDesription} object.
   *
   * @param path The path to the file.
   * @throws IOException if an input or output error occurs.
   * @throws FileNotFoundException if the file is not found.
   */

  public static ChaosGameDescription readFromFile(String path) throws IOException,
      FileNotFoundException, NumberFormatException {
    List<Transform2D> transforms = new ArrayList<>();
    String line;
    try (Scanner reader = new Scanner(new FileReader(path))) {
      String type = checkTransformationType(path);
      switch (type) {
        case "Affine2D" -> {
          Matrix2x2 matrix;
          Vector2D vector;
          Vector2D minCoords = null;
          Vector2D maxCoords = null;
          String[] minCoordsValues;
          String[] maxCoordsValues;
          String[] transformValues;
          int i = 0;
          while (reader.hasNextLine()) {
            line = reader.nextLine();
            int commentIndex = line.indexOf("#");
            // indexOf returns -1 if the # is not found, otherwise it returns the index of the #
            if (commentIndex != -1) {
              // substring from 0 to the index of the #
              line = line.substring(0, commentIndex).trim();
            }
            switch (i) {
              case 0 -> System.out.println(line);
              case 1 -> {
                minCoordsValues = line.split(", ");
                System.out.println(minCoordsValues[0] + " " + minCoordsValues[1]);
                minCoords = new Vector2D(Double.parseDouble(minCoordsValues[0]),
                    Double.parseDouble(minCoordsValues[1]));
              }
              case 2 -> {
                maxCoordsValues = line.split(", ");
                System.out.println(maxCoordsValues[0] + " " + maxCoordsValues[1]);
                maxCoords = new Vector2D(Double.parseDouble(maxCoordsValues[0]),
                    Double.parseDouble(maxCoordsValues[1]));
              }
              default -> {
                try {
                  int newCommentIndex = line.indexOf("#");
                  if (newCommentIndex != -1) {
                    line = line.substring(0, commentIndex).trim();
                  }
                  transformValues = line.split(", ");
                  System.out.println(transformValues[0] + " " + transformValues[1] + " "
                      + transformValues[2] + " " + transformValues[3] + " " + transformValues[4]
                      + " " + transformValues[5]);
                  matrix = new Matrix2x2(Double.parseDouble(transformValues[0]),
                      Double.parseDouble(transformValues[1]),
                      Double.parseDouble(transformValues[2]),
                      Double.parseDouble(transformValues[3]));
                  vector = new Vector2D(Double.parseDouble(transformValues[4]),
                      Double.parseDouble(transformValues[5]));
                  transforms.add(new AffineTransform2D(matrix, vector));
                } catch (NoSuchElementException e) {
                  System.out.println("end of file");
                }
              }
            }
            i++;
          } // end of while loop
          reader.close();
          //System.out.println("liste fra fil: " + transforms);
          return new ChaosGameDescription(minCoords, maxCoords, transforms);
        }

        case "Julia" -> {
          Vector2D minCoords = null;
          Vector2D maxCoords = null;
          Complex complexVector;
          int i = 0;
          while (reader.hasNextLine()) {
            line = reader.nextLine();
            int commentIndex = line.indexOf("#");
            if (commentIndex != -1) {
              line = line.substring(0, commentIndex).trim();
            }
            switch (i) {
              case 0 -> System.out.println(line);
              case 1 -> {
                String[] minCoordsValues = line.split(",");
                System.out.println(minCoordsValues[0] + " " + minCoordsValues[1]);
                minCoords = new Vector2D(Double.parseDouble(minCoordsValues[0]),
                    Double.parseDouble(minCoordsValues[1]));
              }
              case 2 -> {
                String[] maxCoordsValues = line.split(",");
                System.out.println(maxCoordsValues[0] + " " + maxCoordsValues[1]);
                maxCoords = new Vector2D(Double.parseDouble(maxCoordsValues[0]),
                    Double.parseDouble(maxCoordsValues[1]));
              }
              default -> {
                String[] complexValues = line.split(",");
                complexVector = new Complex(Double.parseDouble(complexValues[0]),
                    Double.parseDouble(complexValues[1]));
                // is the imaginary number positive or negative?
                int sign = complexValues[1].indexOf(0);
                transforms.add(new JuliaTransform(complexVector, sign));
                transforms.add(new JuliaTransform(complexVector, - sign));
                System.out.println(complexValues[0] + " " + complexValues[1]);
              }
            }
            i++;
          }
          return new ChaosGameDescription(minCoords, maxCoords, transforms);
        } default -> throw new IllegalArgumentException("Invalid transformation type");
      }
    } catch (IOException e) {
      System.out.println(e.getMessage());
      throw new IOException("Invalid input: " + e.getMessage());
    }
  }


  /**
   * Method was made with help from ChatGPT. ChatGPT was more specifically used to explain what the
   * method needed to do. Also, to explain the use of FileWriter and BufferedWriter. This method
   * writes the description of the chaos game to a file in the specified path. It will check the
   * type of transformation and write the corresponding information to the file.
   *
   * @param description The description of the chaos game.
   * @param path The path to the file.
   * @throws IOException if an input or output error occurs.
   */

  public static void writeToFile(ChaosGameDescription description, String path) throws IOException {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      if (description.getTransforms().getFirst() instanceof AffineTransform2D) {
        writer.write("Affine2D\n");
      } else if (description.getTransforms().getFirst() instanceof JuliaTransform) {
        writer.write("Julia\n");
      }
      String coordinates = description.toString();
      writer.write(coordinates + "\n");
      for (Transform2D t : description.getTransforms()) {
        String tstring = t.toString();
        writer.write(tstring + "\n");
      }
    } catch (IOException e) {
      //System.out.println(e.getMessage());
      throw new IOException("An error occurred while writing to the file: " + e.getMessage());
    }
  }

  /**
   * This method takes in a {@code String} as a parameter, which is the path of the file being read.
   * The method checks if the first line in the file says the transformation is of type;
   * "Julia" or "Affine", and then returns the type in a new String.
   *
   * @param path the String to check from the file, if it contains either "Julia" or "Affine".
   * @return The type of transformation as a string, either "Julia" or "Affine".
   * @throws FileNotFoundException if the file is not found from the path.
   */

  private static String checkTransformationType(String path) throws FileNotFoundException {
    String type = "";
    try (Scanner reader = new Scanner(new FileReader(path))) {
      String line = reader.nextLine();

      int commentIndex = line.indexOf("#");
      if (commentIndex != -1) {
        line = line.substring(0, commentIndex).trim();
      }
      if (line.equalsIgnoreCase("Affine2D")) {
        type = "Affine2D";
      } else if (line.equalsIgnoreCase("Julia")) {
        type = "Julia";
      }
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException("File not found: " + e.getMessage());
    }
    return type;
  }
}
