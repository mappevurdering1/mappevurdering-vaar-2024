package idatt2003.ntnu.mappe.gr37.model;

import idatt2003.ntnu.mappe.gr37.handle.VerifyInput;

/**
 * Vector2D is a class that represents a 2D vector.
 * The vector is used to represent a 2D point in a coordinate system.
 * The vector is defined as (x0, x1).
 * The class contains methods to add, subtract, dot and scale.
 * The class is extended by the {@link Complex} class.
 *
 * @see Complex
 */

public class Vector2D {
  final double x0; //x-coordinate
  final double x1; //y-coordinate

  /**
   * Constructor that initializes the vector with the coordinates x0 and x1.
   * Each coordinate is checked if it is a number and then converted to a double with the help of
   * the methods from the {@link VerifyInput} class.
   *
   * @param x0 x-coordinate
   * @param x1 y-coordinate
   * @throws NumberFormatException if the input is not a number.
   */

  public Vector2D(double x0, double x1) {
    try {
      VerifyInput.verifyInputForNaN(x0);
      VerifyInput.verifyInputForNaN(x1);
      this.x0 = VerifyInput.convertToDouble(String.valueOf(x0));
      this.x1 = VerifyInput.convertToDouble(String.valueOf(x1));
    } catch (IllegalArgumentException e) {
      throw new NumberFormatException();
    }
  }

  /**
   * Getter method for the x-coordinate.
   *
   * @return the x-coordinate.
   */
  public double getX0() {
    return x0;
  }

  /**
   * Getter method for the y-coordinate.
   *
   * @return the y-coordinate.
   */

  public double getX1() {
    return x1;
  }

  /**
   * Adds two vectors together.
   * The addition is defined as (x0, x1) + (y0, y1) = (x0 + y0, x1 + y1).
   *
   * @param other the {@code Vector2D} to add to this vector.
   * @return a {@code Vector2D} that is the sum of the two vectors.
   */

  public Vector2D add(Vector2D other) {
    return new Vector2D(this.x0 + other.getX0(), this.x1 + other.getX1());
  }

  /**
   * Subtracts two vectors.
   * The subtraction is defined as (x0, x1) - (y0, y1) = (x0 - y0, x1 - y1).
   *
   * @param other the vector to subtract from this vector.
   * @return the difference of the two vectors.
   */

  public Vector2D subtract(Vector2D other) {
    return new Vector2D(this.x0 - other.getX0(), this.x1 - other.getX1());
  }

  /**
   * Multiplies two vectors, and return a double value.
   * The multiplication is defined as (x0, x1) * (y0, y1) = (x0 * y0 + x1 * y1).
   *
   * @param other the vector to multiply with this vector.
   * @return a new double value that is the result of the multiplication.
   */

  public double dot(Vector2D other) { // dot product of vectors(multiplication)
    double result = x0 * other.getX0() + x1 * other.getX1();

    return result;
  }

  /**
   * Scales a vector with a scalar.
   * The scaling is defined as scalar * (x0, x1) = (scalar * x0, scalar * x1).
   *
   * @param scalar the scalar to scale the vector with.
   * @return the scaled {@code Vector2D}.
   */

  public Vector2D scalar(double scalar) {
    return new Vector2D(this.x0 * scalar, this.x1 * scalar);
  }
}
