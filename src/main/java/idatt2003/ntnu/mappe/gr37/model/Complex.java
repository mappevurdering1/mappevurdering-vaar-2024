package idatt2003.ntnu.mappe.gr37.model;

import static java.lang.Math.signum;

import idatt2003.ntnu.mappe.gr37.handle.VerifyInput;
import java.lang.Math;

/**
 * This class is a subclass of Vector2D and is used to represent complex numbers.
 * It contains a method that takes in a real part and an imaginary part and returns
 * the square root of the complex number.
 */

public class Complex extends Vector2D {

  /**
   * Constructor that initializes the real part and the imaginary part of the complex number.
   *
   * @param realPart The real number of the complex point.
   * @param imaginaryPart The imaginary number of the complex point.
   */

  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * This method takes in a real number and an imaginary number and returns
   * the square root of the complex point.
   *
   * @return returns a {@code Vector2D} (newRealPart, newImaginaryPart)
   */

  public Complex sqrt() {
    try {
      double realPart = this.getX0();
      double imaginaryPart = this.getX1();
      VerifyInput.verifyInputForNaN(realPart);
      VerifyInput.verifyInputForNaN(imaginaryPart);

      double a = Math.sqrt(Math.pow(realPart, 2) + Math.pow(imaginaryPart, 2));
      double newRealPart = Math.sqrt(0.5 * (a + realPart));
      double newImaginaryPart = signum(imaginaryPart) * (Math.sqrt(0.5 * (a - realPart)));

      VerifyInput.verifyInputForNaN(newRealPart);
      VerifyInput.verifyInputForNaN(newImaginaryPart);

      return new Complex(newRealPart, newImaginaryPart);

    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  /**
   * This method calculates the absolute value of the {@code Complex} point. It was originally
   * used in the implementation of the Mandelbrot set but is not used in the final implementation.
   *
   * @return the absolute value of the {@code Complex} point.
   */

  public double abs() {
    return Math.sqrt((this.getX0() * this.getX0()) + (this.getX1() * this.getX1()));
  }

  /**
   * This method divides a complex number by a double.
   * If the divisor is 0, an ArithmeticException is thrown.
   * The method returns a new complex number with the real part divided by the divisor
   * and the imaginary part divided by the divisor.
   *
   * @param divisor the number to divide the complex number by.
   * @return a new {@code Complex}(newReal, newImaginary)
   * @throws ArithmeticException if the divisor is 0.
   */

  public Complex divide(double divisor) throws ArithmeticException {
    if (divisor == 0) {
      throw new ArithmeticException("Cannot divide by zero");
    }
    double newReal = getX0() / divisor;
    double newImaginary = getX1() / divisor;
    return new Complex(newReal, newImaginary);
  }

  /**
   * This method multiplies a complex number by a double.
   * The method returns a new complex number with the real part multiplied by the factor
   * and the imaginary part multiplied by the factor.
   *
   * @param factor the number to multiply the complex number by.
   * @return a new {@code Complex}(newReal, newImaginary)
   */

  public Complex multiply(double factor) {
    double newReal = getX0() * factor;
    double newImaginary = getX1() * factor;
    return new Complex(newReal, newImaginary);
  }
}



