package idatt2003.ntnu.mappe.gr37.model;

import idatt2003.ntnu.mappe.gr37.handle.VerifyInput;

/**
 * Matrix2x2 is a class that represents a 2x2 matrix.
 * The matrix is used to transform a 2D vector.
 * The matrix is defined as a 2x2 matrix with the elements a00, a01, a10, a11.
 */

public class Matrix2x2 {
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /**
   * Constructor that initializes the matrix with the elements a00, a01, a10, a11.
   * Each element is checked if it is a number and then converted to a double with the help of
   * the methods from the {@link VerifyInput} class.
   *
   * @param a00 The element in the first row and first column.
   * @param a01 The element in the first row and second column.
   * @param a10 The element in the second row and first column.
   * @param a11 The element in the second row and second column.
   * @throws NumberFormatException if the input is not a number.
   */

  public Matrix2x2(double a00, double a01, double a10, double a11) throws NumberFormatException {
    try {
      VerifyInput.verifyInputForNaN(a00);
      VerifyInput.verifyInputForNaN(a01);
      VerifyInput.verifyInputForNaN(a10);
      VerifyInput.verifyInputForNaN(a11);
      this.a00 = VerifyInput.convertToDouble(String.valueOf(a00));
      this.a01 = VerifyInput.convertToDouble(String.valueOf(a01));
      this.a10 = VerifyInput.convertToDouble(String.valueOf(a10));
      this.a11 = VerifyInput.convertToDouble(String.valueOf(a11));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      throw new NumberFormatException();
    }
  }

  /**
   * Multiplies the matrix with a 2D vector.
   * The multiplication is defined as (i, j) = A * (x0, x1).
   * Where A is the 2x2 matrix and (x0, x1) is the 2D vector.
   *
   * @param vector a {@code Vector2D} to multiply with the matrix.
   * @return the transformed {@code Vector2D} point.
   */

  public Vector2D multiply(Vector2D vector) {
    return new Vector2D(a00 * vector.getX0() + a01 * vector.getX1(), a10
        * vector.getX0() + a11 * vector.getX1());
  }

  /**
   * Auto generated getter method with IntelliJ. Accessor method for the element in the first row
   * and first column.
   *
   * @return the element in the first row and first column.
   */

  public double getA00() {
    return a00;
  }

  /**
   * Auto generated getter method with IntelliJ. Accessor method for the element in the first row
   * and second column.
   *
   * @return the element in the first row and second column.
   */

  public double getA01() {
    return a01;
  }

  /**
   * Auto generated getter method with IntelliJ. Accessor method for the element in the second row
   * and first column.
   *
   * @return the element in the second row and first column.
   */

  public double getA10() {
    return a10;
  }

  /**
   * Auto generated getter method with IntelliJ. Accessor method for the element in the second row
   * and second column.
   *
   * @return the element in the second row and second column.
   */

  public double getA11() {
    return a11;
  }
}
