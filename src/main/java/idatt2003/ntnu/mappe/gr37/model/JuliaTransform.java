package idatt2003.ntnu.mappe.gr37.model;

/**
 * JuliaTransform is a class that represents a 2D Julia transformation. The transformation is
 * represented by a complex number and a sign. The transformation is defined as (i, j) = sqrt((x0,
 * x1) - c) * sign. Where c is the complex number and sign is the sign of the imaginary part of the
 * complex number. The transformation is used to transform a 2D vector. The class implements the
 * Transform2D interface to transform a {@code Vector2D} point.
 */

public class JuliaTransform implements Transform2D {
  private Complex point;
  private int sign;

  /**
   * the constructor initializes the point representing the constant Complex object and the sign
   * representing the -1 or 1 depending on the complex number. If there is no valid given sign, it
   * is set to 1, otherwise it is keeping the correct values, either 1 or -1.
   *
   * @param point the Complex constant
   * @param sign the sign, either -1 or 1, respectively.
   */

  public JuliaTransform(Complex point, int sign) {
    this.point = point;
    this.sign = (sign == 1 || sign == -1) ? sign : 1;
  }

  /**
   * This method transforms a 2D vector by taking the square root of the complex number and
   * multiplying it by the sign.
   *
   * @param v the {@code Vector2D} point to transform.
   * @return the transformed {@code Vector2D} point.
   */

  @Override
  public Vector2D transform(Vector2D v) {
    Vector2D subtracted = v.subtract(point);

    Complex radicand = new Complex(subtracted.getX0(), subtracted.getX1());
    Complex z = radicand.sqrt();

    return new Vector2D(z.getX0() * sign, z.getX1() * sign);
  }

  /**
   * Auto generated getter method with IntelliJ. Accessor method for the point of the complex
   * number.
   *
   * @return the point, the complex number.
   */

  public Complex getPoint() {
    return point;
  }

  /**
   * Auto generated getter method with IntelliJ. Accessor method for the sign of the imaginary part
   * of the complex number.
   *
   * @return the sign. Either 1 or -1.
   */

  public int getSign() {
    return sign;
  }

  /**
   * To-string method to represent the Julia transformation. It is used in the
   * {@code ChaosGameFileHandler} to write the transformation to a file.
   *
   * @return the complex number as a string.
   */

  @Override
  public String toString() {
    return point.getX1() + ", " + point.getX0();
  }
}
