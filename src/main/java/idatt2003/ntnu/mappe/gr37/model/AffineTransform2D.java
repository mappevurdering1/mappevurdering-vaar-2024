package idatt2003.ntnu.mappe.gr37.model;


/**
 * AffineTransform2D is a class that represents a 2D affine transformation.
 * The transformation is represented by a 2x2 matrix and a 2D vector.
 * The transformation is defined as (i, j) = A * (x0, x1) + b.
 * Where A is the 2x2 matrix, (x0, x1) is the 2D vector and b is the 2D vector.
 * The transformation is used to transform a 2D vector.
 * The class implements the Transform2D interface.
 *
 * @see Transform2D
 */

public class AffineTransform2D implements Transform2D {
  private final Matrix2x2 matrix;
  private final Vector2D vector;

  /**
   * Constructor that initializes the matrix and vector.
   *
   * @param matrix 2x2 matrix
   * @param vector 2D vector
   */

  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * An affine transformation with a Matrix2x2 and Vector2D. (i, j) = A * (x0, x1) + b.
   *
   * @param x 2D vector
   * @return transformed 2D vector
   */

  @Override
  public Vector2D transform(Vector2D x) {
    return getMatrix().multiply(x).add(getVector());
  }

  /**
   * Returns the matrix of the Affine Transformation.
   *
   * @return the {@link Matrix2x2} of the Affine Transformation.
   */

  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the vector of the Affine Transformation.
   *
   * @return the {@link Vector2D} of the Affine Transformation.
   */

  public Vector2D getVector() {
    return vector;
  }

  /**
   * To-String method for the Affine Transformation showing the coordinates in the matrix and the
   * vector. Respectively, a00, a01, a10, a11, b0, b1.
   *
   * @return A string representation of the Affine Transformation.
   */
  @Override
  public String toString() {
    return matrix.getA00() + ", " + matrix.getA01() + ", " + matrix.getA10()
        + ", " + matrix.getA11() + ", "
        + vector.getX0() + ", " + vector.getX1();
  }
}

