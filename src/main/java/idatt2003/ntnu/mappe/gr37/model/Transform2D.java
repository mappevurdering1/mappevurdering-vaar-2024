package idatt2003.ntnu.mappe.gr37.model;

/**
 * This interface is used to transform a 2D vector. The interface contains a method that takes in a
 * {@code Vector2D} point and returns the transformed {@code Vector2D} point.
 * The interface is implemented by the {@link JuliaTransform} and {@link AffineTransform2D} classes.
 */

public interface Transform2D {

  /**
   * Transforms a 2D vector.
   *
   * @param point 2D vector to transform.
   * @return the transformed 2D vector.
   */
  Vector2D transform(Vector2D point);
}
