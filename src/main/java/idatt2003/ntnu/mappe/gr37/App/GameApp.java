package idatt2003.ntnu.mappe.gr37.App;

import javafx.application.Application;

/**
 * The GameApp class is the main class for the application.
 * It launches the UserInterface class, witch is a JavaFX application.
 *
 * <p>
 *   This class does not hold any methods other than the main method to run the application.
 * </p>
 *
 * @see UserInterface
 */

public class GameApp {

  /**
   * The main method for the application.
   * It launches the UserInterface class.
   *
   * @param args The command line arguments.
   */

  public static void main(String[] args) {
    Application.launch(UserInterface.class);
  }
}
