package idatt2003.ntnu.mappe.gr37.App;

import idatt2003.ntnu.mappe.gr37.button.mainTab.MainTabButtons;
import idatt2003.ntnu.mappe.gr37.button.menu.MenuButtons;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * This class is responsible for creating the user interface of the application.
 * The class extends the Application class to access the JavaFX application and use the start
 * method to create the main scene of the application.
 */

public class UserInterface extends Application {

  MainTab mainTab;
  MainTabButtons mainButtons;
  MenuButtons menu;
  Stage stage;
  static Stage makeNewTransform;

  /**
   * This is the start method for the application.
   * It creates the main scene with the top row, bottom row and menu buttons.
   *
   * @param primaryStage The {@code Stage} for the application.
   */

  @Override
  public void start(Stage primaryStage) {
    this.mainButtons = new MainTabButtons(this);
    this.menu = new MenuButtons();
    this.mainTab = new MainTab(mainButtons, menu);
    this.stage = primaryStage;
    MainTab.setTopRow(MainTab.topRow());
    makeNewTransform = new Stage();

    primaryStage.setTitle("Chaos Game");
    primaryStage.setScene(createMainScene());
    primaryStage.setResizable(true);
    primaryStage.setMinWidth(725);
    primaryStage.setMinHeight(658);
    primaryStage.show();
  }

  /**
   * This method accesses the new Transformation stage to let the user type in the new
   * transformation information in a separate window.
   *
   * @return The new {@code Stage}.
   */

  public static Stage getNewTransformStage() {
    return makeNewTransform;
  }

  /**
   * Creates the main scene of the application with its set dimensions, and declares the
   * stylesheet.
   *
   * @return the main scene.
   */

  public Scene createMainScene() {
    Scene scene = new Scene(mainTab.createMainTab(), 1100, 700);
    scene.getStylesheets().add("styles.css");
    return scene;
  }

  /**
   * popUpError creates a pop-up error with the given error message.
   * The pop-up window is an Alert window with the type Error. It is mostly used to handle errors
   * where it is not possible to continue the action, and let the user know what went wrong.
   *
   * @param errorMessage The message to be displayed in the pop-up window.
   */

  public static void popUpError(String errorMessage) {
    Alert error = new Alert(Alert.AlertType.ERROR);
    error.setTitle("Error!");
    error.setHeaderText("There was something wrong with your action...");
    error.setContentText(errorMessage);
    error.showingProperty();
    error.showAndWait();
  }

  /**
   * popUpMessage creates a pop-up message with the given message.
   * The pop-up window is an Alert window with the type Information. The desired message will be
   * displayed as the header of the pop-up window.
   *
   * @param message The message to be displayed in the pop-up window.
   */

  public static void popUpMessage(String message) {
    Alert error = new Alert(Alert.AlertType.INFORMATION);
    error.setTitle("Message:");
    error.setHeaderText(message);
    error.showingProperty();
    error.showAndWait();
  }

  public MainTab getMainTab() {
    return mainTab;
  }

}
