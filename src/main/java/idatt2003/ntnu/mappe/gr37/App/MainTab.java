package idatt2003.ntnu.mappe.gr37.App;

import idatt2003.ntnu.mappe.gr37.button.mainTab.CanvasView;
import idatt2003.ntnu.mappe.gr37.button.mainTab.MainTabButtons;
import idatt2003.ntnu.mappe.gr37.button.menu.MenuButtons;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * This class is responsible for creating the main tab in the GUI.
 * The main tab contains the canvas, buttons and menu, and is the main view of the application.
 *
 * <p>
 *   This class creates the main tab in the GUI.
 *   As well as the menu, top row, bottom row, and canvas.
 *   It also provides methods to clear the scene and get the path.
 * </p>
 *
 * @see MainTabButtons
 * @see MenuButtons
 */

public class MainTab {
  protected static BorderPane layout;
  private MainTabButtons button;
  private MenuButtons menu;
  private static Canvas canvas;

  /**
   * Constructor for the MainTab class. It is used in the UserInterface class to create the main
   * tab.
   *
   * @param button The main tab buttons
   * @param leftMenu The menu buttons
   */

  public MainTab(MainTabButtons button, MenuButtons leftMenu) {
    this.layout = new BorderPane();
    this.button = button;
    this.menu = leftMenu;
    canvas = CanvasView.getCanvas();
  }

  /**
   * Accessor method for the canvas.
   *
   * @return canvas
   */

  public static Canvas getCanvas() {
    return canvas;
  }

  /**
   * Creates the main tab in the GUI.
   * The main tab contains the canvas, bottom row exit button, the title or the edit transformation
   * functionality, and the menu.
   *
   * @return The {@code BorderPane} layout.
   */

  public BorderPane createMainTab() {
    layout.getStyleClass().add("main-tab");
    layout.setTop(topRow());
    layout.setLeft(createMenu());

    VBox vbox = new VBox();
    vbox.getChildren().add(botRow());
    VBox.setVgrow(botRow(), Priority.ALWAYS);
    layout.setBottom(vbox);

    layout.setCenter(getCanvas());

    return layout;
  }

  /**
   * Updates the center pane in the main tab. This method is used to update the canvas to the user.
   *
   * @param canvas The {@code Canvas} to update the center pane with.
   */

  public static void updateCenterPane(Canvas canvas) {
    getPane().setCenter(canvas);
  }

  /**
   * Accessor method for the {@code BorderPane} layout of the main tab.
   *
   * @return the {@code Borderpane} layout.
   */

  public static BorderPane getPane() {
    return layout;
  }

  /**
   * Creates the menu in the main tab, in a VBox.
   * This menu contains buttons for creating new Transformations, and presets for Julia,
   * Barnsley and Sierpinski fractals, as well as buttons for reading from file and saving image.
   *
   * @return the {@code VBox} menuBox.
   */

  private VBox createMenu() {
    VBox menuBox = new VBox();
    menuBox.getStyleClass().add("menu-box");
    menuBox.fillWidthProperty();
    menuBox.getChildren().addAll(
        menu.createNewJulia(),
        menu.createNewBarnsley(),
        menu.createNewSierpinski(),
        menu.createNewTransformation(),
        menu.readFromFile(),
        menu.saveImage());
    menuBox.setSpacing(10);
    menuBox.getChildren().forEach(b -> {
      b.getStyleClass().add("button");
    });
    return menuBox;
  }

  /**
   * Creates the bottom row in the main tab, in a HBox.
   * This row contains a button for exiting the game.
   *
   * @return {@code HBox} bottom.
   */

  private HBox botRow() {
    HBox bottom = new HBox();
    bottom.getStyleClass().add("bottom-row");
    bottom.getChildren().addAll(button.exitGameButton());

    return bottom;
  }

  /**
   * Creates the top row in the main tab with the project title, in a HBox.
   * This row contains the title of the game, or the edit functionality for each predefined
   * transformation.
   *
   * @return {@code HBox} top.
   */

  public static HBox topRow() {
    HBox top = new HBox();
    top.getStyleClass().add("title-row");
    top.getChildren().add(getTitle());
    top.setMinHeight(30);
    return top;
  }

  /**
   * Sets the top row in the main tab with the desired {@code HBox}.
   *
   * @param topRow The new {@code HBox} to set as top row.
   */

  public static void setTopRow(HBox topRow) {
    layout.setTop(topRow);
  }

  /**
   * Accesses the title of the game.
   *
   * @return {@code Text} title.
   */

  public static Text getTitle() {
    Text title = new Text("Welcome to the Chaos Game <3");
    title.getStyleClass().add("title");
    return title;
  }


  /**
   * Clears the scene by removing all nodes from the {@code BorderPane} layout.
   */

  public void clearScene() {
    layout.getChildren().clear();
  }
}
