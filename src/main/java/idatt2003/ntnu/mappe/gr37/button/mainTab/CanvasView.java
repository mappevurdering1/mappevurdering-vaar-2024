package idatt2003.ntnu.mappe.gr37.button.mainTab;

import idatt2003.ntnu.mappe.gr37.Main;
import idatt2003.ntnu.mappe.gr37.game.ChaosCanvas;
import idatt2003.ntnu.mappe.gr37.game.ChaosGame;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The CanvasView class is responsible for creating the canvas view in the GUI.
 * The canvas view contains the canvas, and is the main view of the application.
 *
 * @see Canvas
 * @see GraphicsContext
 */

public class CanvasView {

  static Canvas canvas = new Canvas(500, 500);
  static GraphicsContext gc = canvas.getGraphicsContext2D();

  /**
   * Constructor for the CanvasView class.
   */

  public CanvasView() {
  }

  /**
   * drawPixel draws a pixel on the canvas.
   * The pixel is drawn at the specified point, from the parameter.
   * If the GraphicsContext is null, a NullPointerException is thrown.
   * If the point is outside the canvas, an IllegalArgumentException is thrown.
   * The scaling factor is set to 8.0, to make the image more visible.
   * The pixel is drawn at the specified point, with a width and height of 2, in black.
   *
   * @param point The point to draw the pixel at.
   *              The point is a Vector2D object, with the x and y coordinates of the point.
   *
   * @throws NullPointerException If the GraphicsContext is null.
   * @throws IllegalArgumentException If the point is outside the canvas.
   */

  public static void drawPixel(Vector2D point) {
    if (gc == null) {
      throw new NullPointerException("GraphicsContext is null");
    }
    double scalingFactor = 9.0; // todo: wrap med try catch om noe kommer utenfor
    double scaledX = point.getX0() * scalingFactor; //i
    double scaledY = point.getX1() * scalingFactor; //j

    gc.setFill(Color.BLACK); // todo: denne styrer fargen på pixlene
    gc.fillRect(scaledX, scaledY, 2, 2);
  }

  /**
   * The drawCanvas() method takes in a canvas array and checks if the graphic content on that array
   * is {@code null}. If it is, it will throw a NullPointerException. If it is not null, it will
   * continue to draw a pixel on the point where the value of the canvas array != 0.
   *
   * @param canvasArray array on the canvas.
   */

  public static void drawCanvas(int[][] canvasArray) {
    if (gc == null) {
      throw new NullPointerException("GraphicsContext is null");
    }
    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    for (int i = 0; i < canvasArray.length; i++) {
      for (int j = 0; j < canvasArray[i].length; j++) {
        if (canvasArray[i][j] != 0) {
          drawPixel(new Vector2D(i, j));
        }
      }
    }
  }

  /**
   * createCanvas creates a canvas with the specified ChaosGameDescription and steps to run the
   * game. The canvas is drawn with the drawCanvas() method, and the {@code Canvas} is returned.
   *
   * @param description ChaosGameDescription to draw on the canvas.
   * @throws NullPointerException If the GraphicsContext is null.
   */

  public static Canvas createCanvas(ChaosGameDescription description, int steps) {
    gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    gc = canvas.getGraphicsContext2D();
    ChaosGame game = Main.runGame(description, steps);
    ChaosCanvas chaosCanvas = game.getCanvas();
    int[][] canvasArray = chaosCanvas.getCanvasArray();
    drawCanvas(canvasArray);

    return canvas;
  }

  /**
   * getCanvas returns the {@code Canvas}.
   *
   * @return the canvas
   */

  public static Canvas getCanvas() {
    return canvas;
  }
}