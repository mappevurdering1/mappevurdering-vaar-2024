package idatt2003.ntnu.mappe.gr37.button.mainTab;

import idatt2003.ntnu.mappe.gr37.App.UserInterface;
import javafx.scene.control.Button;

/**
 * The MainTabButtons class is responsible for creating the buttons for the main tab. This class
 * contains the exit game button. The exit game button closes the application when clicked. This
 * class was originally planned to contain more buttons, but the scope of the project was reduced.
 *
 * @see UserInterface
 */

public class MainTabButtons {

  private final UserInterface ui;

  /**
   * Constructor for the MainTabButtons class initializing the {@code UserInterface} object.
   *
   * @param ui The user interface.
   */

  public MainTabButtons(UserInterface ui) {
    this.ui = ui;
  }

  /**
   * Creates the exit game button.
   *
   * @return The exit game button.
   */

  public Button exitGameButton() {
    Button exit = new Button("Exit");
    exit.setOnAction(e -> {
      ui.getMainTab().clearScene();
      System.exit(0);
    });
    return exit;
  }
}
