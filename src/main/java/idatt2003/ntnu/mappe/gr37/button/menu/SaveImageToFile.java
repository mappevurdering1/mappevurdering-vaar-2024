package idatt2003.ntnu.mappe.gr37.button.menu;

import idatt2003.ntnu.mappe.gr37.App.UserInterface;
import java.awt.image.RenderedImage;
import java.io.File;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;

/**
 * This class is responsible for creating the functionality of saving the canvas to a .png image
 * file.
 */

public class SaveImageToFile {

  /**
   * This method saves the image to a file. It takes a canvas and a file name as parameters,
   * and saves the image to a png file, and saves it in the resources folder of the project.
   *
   * @param canvas The canvas to save as an image.
   * @param fileName The name of the file to save the image to.
   */

  protected static void saveImageToFile(Canvas canvas, String fileName) {
    WritableImage writableImage =
        new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
    canvas.snapshot(new SnapshotParameters(), writableImage);
    File file = new File("src/main/resources/" + fileName);
    try {
      RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
      ImageIO.write(renderedImage, "png", file);
    } catch (Exception e) {
      UserInterface.popUpError(e.getMessage());
    }
  }
}
