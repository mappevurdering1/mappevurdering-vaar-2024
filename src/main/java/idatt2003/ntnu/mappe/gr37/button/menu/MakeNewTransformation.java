package idatt2003.ntnu.mappe.gr37.button.menu;

import idatt2003.ntnu.mappe.gr37.App.UserInterface;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameFileHandler;
import idatt2003.ntnu.mappe.gr37.handle.VerifyInput;
import idatt2003.ntnu.mappe.gr37.model.AffineTransform2D;
import idatt2003.ntnu.mappe.gr37.model.Complex;
import idatt2003.ntnu.mappe.gr37.model.JuliaTransform;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The MakeNewTransformation class is responsible for creating a new transformation.
 * The class contains methods to create a new transformation, and to check the input values.
 *
 * @see UserInterface
 */

public class MakeNewTransformation {
  /**
   * list of all transformations added.
   */
  private static List<Double> affineTransformations = new ArrayList<>();
  /**
   * List of all julia transformations added.
   */
  private static List<Double> juliaTransformations = new ArrayList<>();
  /**
   * List of final minimum and maximum coordinates from the user inputs.
   */
  private static List<Double> minMaxCoords = new ArrayList<>();
  /**
   * List of the chosen min and max coords for the transformations.
   */
  private static List<Double> minMaxValues = new ArrayList<>();
  /**
   * List of temporary input values to be added to the affineTransformations list.
   */
  private static List<Double> affineValues = new ArrayList<>();
  /**
   * List of temporary input values to be added to the juliaTransformations list.
   */
  private static List<Double> juliaValues = new ArrayList<>();

  /**
   * Constructor for the MakeNewTransformation class.
   */

  public MakeNewTransformation() {
  }

  /**
   * getAffineValues() returns the affine values.
   *
   * @return List of all {@code Double} values in the affine transformation.
   */

  public static List<Double> getAffineTransformations() {
    return affineTransformations;
  }

  /**
   * getJuliaValues() returns the julia values.
   *
   * @return List of all {@code Double} values in the julia transformation.
   */

  public static List<Double> getJuliaTransformations() {
    return juliaTransformations;
  }

  /**
   * getCoordValues() returns the min and max coordinates.
   *
   * @return List with {@code Double} values of min and max coordinates.
   */

  public static List<Double> getCoords() {
    return minMaxCoords;
  }

  /**
   * Access method for the last affine transformation values.
   *
   * @return a list of {@code Double} values.
   */

  public static List<Double> getAffineValues() {
    return affineValues;
  }

  /**
   * Access method for the list of the last input of julia values.
   *
   * @return a list of {@code Double} values.
   */

  public static List<Double> getJuliaValues() {
    return juliaValues;
  }

  /**
   * getNewScene() returns a new scene.
   *
   * @param parent The parent pane.
   * @return A new scene.
   */

  public static Scene getNewScene(Pane parent) {
    return new Scene(parent, 500, 350);
  }

  /**
   * createNewTransformation() creates a new transformation.
   *
   * @param box The VBox to add to the transformation.
   * @return A new transformation.
   */

  public static BorderPane createNewTransformation(VBox box) {
    BorderPane pane = new BorderPane();
    VBox main = new VBox();
    pane.getStylesheets().add("styles.css");
    main.getStyleClass().add("pop-up-window");
    main.getChildren().add(box);
    pane.setCenter(main);

    return pane;
  }

  /**
   * chooseTransform() creates a choice box to choose between Julia and Affine transformations.
   *
   * @return A VBox with a choice box.
   */

  public static VBox chooseTransform() {
    VBox box = new VBox();
    box.getStyleClass().add("pop-up-window");
    HBox input = new HBox();
    input.getStyleClass().add("pop-up-window");

    ChoiceBox<String> choice = new ChoiceBox<>();
    choice.setValue("Choose...");
    choice.getItems().addAll("Julia", "Affine");
    box.getChildren().addAll(choice);
    box.getChildren().add(input);
    choice.getSelectionModel().selectedItemProperty()
        .addListener((observable, oldValue, newValue) -> {
          if (newValue.equals("Julia")) {
            input.getChildren().clear();
            input.getChildren().add(newCoords(true));
          }
          if (newValue.equals("Affine")) {
            input.getChildren().clear();
            input.getChildren().add(newCoords(false));
          }
          if (newValue.equals("Choose...")) {
            input.getChildren().clear();
          }
        });
    return box;
  }

  /**
   * getAffineBox() returns a VBox with the affine transformation input fields.
   *
   * @return A VBox with the affine transformation input fields.
   */

  private static VBox getAffineBox() {
    TextField a00 = new TextField();
    TextField a01 = new TextField();
    TextField a10 = new TextField();
    TextField a11 = new TextField();
    TextField b0 = new TextField();
    TextField b1 = new TextField();
    Text matrixText = new Text("Matrix numbers:");
    Text vectorText = new Text("Vector numbers:");
    matrixText.getStyleClass().add("small-text");
    vectorText.getStyleClass().add("small-text");
    List<TextField> fields = new ArrayList<>();
    Collections.addAll(fields, a00, a01, a10, a11, b0, b1);
    fields.forEach(f -> f.setText("0.0"));
    addListener(fields, affineTransformations);

    VBox matrixBox = new VBox();
    VBox vectorBox = new VBox();
    VBox vector = new VBox();
    HBox topBox = new HBox();
    HBox botBox = new HBox();
    topBox.getStyleClass().add("pop-up-window");
    botBox.getStyleClass().add("pop-up-window");
    topBox.getChildren().addAll(a00, a01);
    botBox.getChildren().addAll(a10, a11);
    vector.getStyleClass().add("pop-up-window");
    vector.getChildren().addAll(b0, b1);
    vectorBox.getChildren().addAll(vectorText, vector);
    matrixBox.getChildren().addAll(matrixText, topBox, botBox);
    HBox content = new HBox();
    content.getChildren().addAll(matrixBox, vectorBox);
    HBox buttons = saveCancelButtons();
    buttons.getChildren().addAll(MenuButtons.nextButton(false, affineValues),
        MenuButtons.saveAffineInput());
    VBox affineBox = new VBox();
    affineBox.getChildren().addAll(content, buttons);

    return affineBox;
  }

  /**
   * juliaBox() returns a VBox with the Julia transformation input fields.
   *
   * @return A VBox with the Julia transformation input fields.
   */

  private static VBox juliaBox() {
    VBox imagBox = new VBox();
    VBox realBox = new VBox();
    imagBox.getStyleClass().add("pop-up-window");
    realBox.getStyleClass().add("pop-up-window");
    List<TextField> fields = new ArrayList<>();
    TextField imaginary = new TextField();
    TextField real = new TextField();
    Collections.addAll(fields, real, imaginary);
    fields.forEach(f -> f.setText("0.0"));
    addListener(fields, juliaTransformations);

    Text i = new Text("Imaginary number");
    Text r = new Text("Real number");
    i.getStyleClass().add("small-text");
    r.getStyleClass().add("small-text");

    imagBox.getChildren().addAll(r, real);
    realBox.getChildren().addAll(i, imaginary);
    VBox container = new VBox();
    HBox hbox = new HBox();
    hbox.getChildren().addAll(imagBox, realBox);
    HBox buttons = saveCancelButtons();
    buttons.getChildren().add(MenuButtons.saveJuliaInput());
    container.getChildren().addAll(hbox, buttons);

    return container;
  }

  /**
   * saveCancelButtons() returns a HBox with the save and cancel buttons.
   *
   * @return A HBox with the save and cancel buttons.
   */

  private static HBox saveCancelButtons() {
    HBox buttons = new HBox();
    buttons.getStyleClass().add("pop-up-window");
    buttons.getChildren().add(MenuButtons.cancel());
    return buttons;
  }

  /**
   * makeNewJulia() creates a new Julia transformation.
   *
   * @param real The real part of the Julia transformation.
   * @param imaginary The imaginary part of the Julia transformation.
   * @return A new Julia transformation.
   */

  public static JuliaTransform makeNewJulia(double real, double imaginary)  {
    if (imaginary < 0) {
      return new JuliaTransform(new Complex(real, imaginary), -1);
    } else {
      return new JuliaTransform(new Complex(real, imaginary), 1);
    }
  }

  /**
   * MakeNewAffine() creates a new Affine transformation.
   *
   * @param matrix the matrix for the transformation
   * @param vector the vector for the transformation
   * @return a new Affine transformation
   */

  public static AffineTransform2D makeNewAffine(Matrix2x2 matrix, Vector2D vector) {
    return new AffineTransform2D(matrix, vector);
  }

  /**
   * makeNewDescription() creates a new ChaosGameDescription from the input of the user. It takes
   * in the List of transformations the user has chosen, as well as the lower left and upper right
   * coordinates of the canvas for the transformation.
   *
   * @param transforms the transformations for the desired ChaosGameDescription
   * @param min the minimum coordinates for the ChaosGameDescription
   * @param max the maximum coordinates for the ChaosGameDescription
   * @return a new ChaosGameDescription
   */

  public static ChaosGameDescription makeNewDescription(List<Transform2D> transforms, Vector2D min,
                                                        Vector2D max) {
    return new ChaosGameDescription(min, max, transforms);
  }

  /**
   * The method takes in a list of {@code TextFields} to add listeners to. And the list which all
   * new values will be added to in the end. In this method, the transformsList parameter is only
   * used to check which value list needs to be initialized with values. And then the method will
   * call the {@code transferFieldsToList()} method with the correct value list.
   *
   * @param fields List of TextFields to iterate through to add listener.
   * @param transformsList the list which will contain all final values of the transformations.
   * @throws IllegalArgumentException throws exception if the new value is not convertible to a
   *          double value, or if the index of the list being iterated is out of bounds.
   */

  private static void addListener(List<TextField> fields, List<Double> transformsList)
      throws IllegalArgumentException {
    if (transformsList == juliaTransformations) {
      juliaValues.clear();
      juliaValues = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 2) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, juliaValues);

    } else if (transformsList == affineTransformations) {
      affineValues.clear();
      affineValues = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 6) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, affineValues);

    } else if (transformsList == minMaxCoords) {
      minMaxValues.clear();
      minMaxValues = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 4) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, minMaxValues);
    }
  }

  /**
   * This method was made with help from GitHub Copilot Chat to help define the TextField to only
   * accept numbers. The method takes in a list of {@code TextFields} to add listeners to.
   * The text fields can only hold numbers and has a specific index for each value. Therefore,
   * each index is transferred to the new list that hold the {@code Double} new values from the
   * text fields.
   *
   * @param fields List of TextFields to iterate through to add listener.
   * @param valueList the list which the values from the text field will be added to.
   * @throws IllegalArgumentException throws exception if the new value is not convertible to a
   *          double value, or if the index of the list being iterated is out of bounds.
   */

  private static void transferFieldsToList(List<TextField> fields, List<Double> valueList) {
    for (TextField f : fields) {
      int index = fields.indexOf(f);
      f.textProperty().addListener((observable, old, newValue) -> {
        if (newValue.matches("-?\\d+(\\.\\d+)?")) {
          try {
            double value = VerifyInput.convertToDouble(newValue);
            valueList.set(index, value);
          } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
          }
        }
        System.out.println(valueList);
      });
    }
  }

  /**
   * This method takes in a list of values from the user inputs and adds them to the respective list
   * of transformations, or list with min and max coordinates.
   *
   * @param valueList the list with values to add to the final list.
   */

  protected static void addValuesToList(List<Double> valueList) {
    if (valueList == minMaxValues) {
      minMaxCoords.addAll(valueList);
    } else if (valueList == juliaValues) {
      juliaTransformations.addAll(valueList);
    } else {
      affineTransformations.addAll(valueList);
    }
  }

  /**
   * checkList() checks if the list is empty, if all elements are 0.0, and if the imaginary number.
   *
   * @param list List of double values to check.
   * @throws IllegalArgumentException throws exception if the list is empty, if all elements are
   *        0.0, or if the imaginary number is 0.0.
   *
   */

  protected static void checkList(List<Double> list) throws IllegalArgumentException {
    boolean all0 = true;
    if (!list.isEmpty()) {
      for (Double value : list) {
        if (!value.equals(0.0)) {
          all0 = false;
          break;
        }
      }
    } else {
      throw new IllegalArgumentException("Please insert numbers in all input fields.");
    }
    if ((list == juliaValues) && (juliaValues.getFirst() == 0.0)) {
      throw new IllegalArgumentException("The imaginary number cannot be: 0.0");
    }
    if (all0) {
      throw new IllegalArgumentException("All elements cannot be: 0.0");
    }
  }

  /**
   * checkCoords() checks if the min and max coordinates are equal.
   *
   * @param list List of double values to check.
   * @throws IllegalArgumentException throws exception if the min and max coordinates are equal.
   */

  protected static void checkCoords(List<Double> list) throws IllegalArgumentException {
    if (list.getFirst().equals(list.get(2)) && list.get(1).equals(list.getLast())) {
      throw new IllegalArgumentException("The min and max coordinates cannot be equal.");
    }
  }

  /**
   * newCoords() creates a VBox with the min and max coordinates for the transformation.
   *
   * @return A VBox with the min and max coordinates for the transformation.
   */

  private static VBox newCoords(boolean isJulia) {
    TextField minCoord0 = new TextField();
    TextField minCoord1 = new TextField();
    TextField maxCoord0 = new TextField();
    TextField maxCoord1 = new TextField();

    List<TextField> fields = new ArrayList<>();
    Collections.addAll(fields, minCoord0, minCoord1, maxCoord0, maxCoord1);
    fields.forEach(f -> f.setPromptText("0.0"));
    VBox minVectors = new VBox();
    VBox maxVectors = new VBox();
    minVectors.getStyleClass().add("pop-up-window");
    maxVectors.getStyleClass().add("pop-up-window");
    Text title = new Text("Choose min & max coordinates for the transformation:");
    Text minText = new Text("Min coordinate");
    Text maxText = new Text("Max coordinate");

    List<Text> textList = new ArrayList<>();
    Collections.addAll(textList, title, minText, maxText);
    textList.forEach(t -> t.getStyleClass().add("small-text"));
    HBox buttons = saveCancelButtons();
    addListener(fields, minMaxCoords);
    if (isJulia) {
      buttons.getChildren().addAll(MenuButtons.nextButton(true, minMaxValues));
    } else {
      buttons.getChildren().addAll(MenuButtons.nextButton(false, minMaxValues));
    }
    VBox container = new VBox();
    HBox inputs = new HBox();
    minVectors.getChildren().addAll(minText, minCoord0, minCoord1);
    maxVectors.getChildren().addAll(maxText, maxCoord0, maxCoord1);
    inputs.getChildren().addAll(minVectors, maxVectors);
    container.getChildren().addAll(title, inputs, buttons);

    return container;
  }

  /**
   * writeTransformation() writes the new ChaosGameDescription from the user input to a file.
   *
   * @param description is a newly created ChaosGameDescription object to write to file.
   */

  protected static void writeTransformation(ChaosGameDescription description) {
    try {
      String path = "src/main/resources/idatt2023.ntnu.mappe.gr37.files/description.txt";
      ChaosGameFileHandler.writeToFile(description, path);
    } catch (IOException e) {
      UserInterface.popUpError(e.getMessage());
    }
  }

  /**
   * clearAndMakeNewScene() clears the scene and creates a new scene.
   *
   * @param isJulia boolean value to check if the transformation is a Julia transformation.
   */

  protected static void clearAndMakeNewScene(boolean isJulia) {
    UserInterface.getNewTransformStage().getProperties().clear();
    if (isJulia) {
      UserInterface.getNewTransformStage().setScene(getNewScene(
          createNewTransformation(juliaBox())));
    } else {
      UserInterface.getNewTransformStage().setScene(getNewScene(
          createNewTransformation(getAffineBox())));
    }
  }
}
