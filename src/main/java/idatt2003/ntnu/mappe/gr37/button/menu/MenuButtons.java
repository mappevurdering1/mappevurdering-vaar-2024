package idatt2003.ntnu.mappe.gr37.button.menu;

import idatt2003.ntnu.mappe.gr37.App.MainTab;
import idatt2003.ntnu.mappe.gr37.App.UserInterface;
import idatt2003.ntnu.mappe.gr37.Factory.BarnsleyFernDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.Factory.JuliaDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.Factory.SierpinskiDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.button.mainTab.CanvasView;
import idatt2003.ntnu.mappe.gr37.button.mainTab.TopRowButtons;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;


/**
 * The MenuButtons class is responsible for creating the buttons for the menu.
 * The menu buttons are used to create new games and transformations, save images, and read
 * descriptions from files.
 */

public class MenuButtons {

  /**
   * The constructor of the MenuButtons class.
   */

  public MenuButtons() {
  }

  /**
   * Creates the button for creating a new julia transformation.
   *
   * @return The new julia button.
   */

  public Button createNewJulia() {

    Button newJulia = new Button("Julia");
    newJulia.setOnAction(j -> {
      try {
        MainTab.setTopRow(TopRowButtons.createTopRow(TopRowButtons.createJulia()));
        ChaosGameDescription description = new JuliaDescriptionFactory().createGameDescription();
        MainTab.updateCenterPane(CanvasView.createCanvas(description, 15000));

      } catch (Exception e) {
        UserInterface.popUpError(e.getMessage());
      }
    });
    return newJulia;
  }

  /**
   * Creates the button for creating a new barnsley transformation.
   *
   * @return The new barnsley button.
   */

  public Button createNewBarnsley() {
    Button newBarnsley = new Button("Barnsley");
    newBarnsley.setOnAction(b -> {
      try {
        MainTab.setTopRow(TopRowButtons.createTopRow(TopRowButtons.createAffine(false)));
        ChaosGameDescription description = new BarnsleyFernDescriptionFactory()
            .createGameDescription();
        MainTab.updateCenterPane(CanvasView.createCanvas(description, 15000));

      } catch (Exception e) {
        UserInterface.popUpError(e.getMessage());
      }
    });
    return newBarnsley;
  }

  /**
   * Creates the button for creating a new sierpinski transformation.
   *
   * @return The new sierpinski button.
   */

  public Button createNewSierpinski() {
    Button newSierpinski = new Button("Sierpinski");
    newSierpinski.setOnAction(s -> {
      try {
        MainTab.setTopRow(TopRowButtons.createTopRow(TopRowButtons.createAffine(true)));
        ChaosGameDescription description =
            new SierpinskiDescriptionFactory().createGameDescription();
        MainTab.updateCenterPane(CanvasView.createCanvas(description, 15000));

      } catch (Exception e) {
        UserInterface.popUpError(e.getMessage());
      }
    });
    return newSierpinski;
  }

  /**
   * The saveImage() method saves the image to a file. It takes a canvas and a file name as
   * parameters, and saves the image to a png file, and saves it in the resources folder of the
   * project.
   *
   * @return The save image button.
   */

  public Button saveImage() {
    Button saveImage = new Button("Save Image");
    saveImage.setOnAction(s -> {
      try {
        SaveImageToFile.saveImageToFile(MainTab.getCanvas(), "image.png");
        UserInterface.popUpMessage("When the application is closed, you can find your"
            + " image in the resources folder, named: image.png");

      } catch (Exception e) {
        UserInterface.popUpError(e.getMessage());
      }
    });

    return saveImage;
  }

  /**
   * Creates the button for creating a new transformation.
   *
   * @return The new transformation button.
   */

  public Button createNewTransformation() {

    Button newTransform = new Button("New Transformation");
    newTransform.setOnAction(e -> {
      MainTab.setTopRow(MainTab.topRow());
      try {
        MakeNewTransformation.getAffineTransformations().clear();
        MakeNewTransformation.getJuliaTransformations().clear();
        UserInterface.getNewTransformStage().setScene(MakeNewTransformation
            .getNewScene(MakeNewTransformation.createNewTransformation(
                MakeNewTransformation.chooseTransform())));
        UserInterface.getNewTransformStage().show();
      } catch (IllegalArgumentException t) {
        UserInterface.popUpError(t.getMessage());
      }
    });
    return newTransform;
  }

  /**
   * Creates the button for saving the input for a new julia transformation.
   *
   * @return The save button.
   */

  protected static Button saveJuliaInput() {
    Button save = new Button("Save");
    save.getStyleClass().add("button");
    save.setOnAction(t -> {
      try {
        // save julia transformations:
        List<Transform2D> transforms = new ArrayList<>();
        List<Double> lastTransformation = MakeNewTransformation.getJuliaValues();
        MakeNewTransformation.checkList(lastTransformation);
        MakeNewTransformation.addValuesToList(lastTransformation);
        List<Double> values = MakeNewTransformation.getJuliaTransformations();
        for (int i = 0; i < values.size(); i += 2) {
          double real = values.get(i);
          double imag = values.get(i + 1);
          transforms.add(MakeNewTransformation.makeNewJulia(real, imag));
        }
        //System.out.println(transforms);

        // save coordinates:
        List<Double> coordValues = MakeNewTransformation.getCoords();
        MakeNewTransformation.checkCoords(coordValues);
        Vector2D minCoords = new Vector2D(coordValues.getFirst(), coordValues.get(1));
        Vector2D maxCoords = new Vector2D(coordValues.get(2), coordValues.getLast());

        // make a new description:
        ChaosGameDescription desc =
            MakeNewTransformation.makeNewDescription(transforms, minCoords, maxCoords);
        // write result to file:
        MakeNewTransformation.writeTransformation(desc);
        // show:
        UserInterface.popUpMessage("You can now run your transformation"
            + " from the List in the menu");
        UserInterface.getNewTransformStage().close();
      } catch (IllegalArgumentException e) {
        UserInterface.popUpError(e.getMessage());
      }
    });
    return save;
  }

  /**
   * Creates the button for saving the input for a new barnsley transformation.
   *
   * @return The save button.
   */

  protected static Button saveAffineInput() {
    Button save = new Button("Save");
    save.getStyleClass().add("button");
    save.setOnAction(t -> {
      try {
        // save affine transformation values:
        List<Transform2D> transforms = new ArrayList<>();
        List<Double> lastTransformation = MakeNewTransformation.getAffineValues();
        MakeNewTransformation.checkList(lastTransformation);
        MakeNewTransformation.addValuesToList(lastTransformation);
        List<Double> values = MakeNewTransformation.getAffineTransformations();
        List<Double> transformValues = new ArrayList<>();
        for (int i = 0; i < values.size(); i += 6) {
          for (int j = 0; j < 6; j++) {
            transformValues.add(values.get(i + j));
          }
          Matrix2x2 m = new Matrix2x2(transformValues.getFirst(), transformValues.get(1),
              transformValues.get(2), transformValues.get(3));
          Vector2D b = new Vector2D(transformValues.get(4), transformValues.getLast());
          transforms.add(MakeNewTransformation.makeNewAffine(m, b));
          transformValues.clear();
        }
        //System.out.println(transforms);
        // save coordinates:
        List<Double> coordValues = MakeNewTransformation.getCoords();
        MakeNewTransformation.checkCoords(coordValues);
        Vector2D minCoords = new Vector2D(coordValues.getFirst(), coordValues.get(1));
        Vector2D maxCoords = new Vector2D(coordValues.get(2), coordValues.getLast());

        ChaosGameDescription desc =
            MakeNewTransformation.makeNewDescription(transforms, minCoords, maxCoords);
        // write the new transformation to the file:
        MakeNewTransformation.writeTransformation(desc);
        // show:
        UserInterface.popUpMessage("You can now run your transformation"
            + " from the List in the menu");
        UserInterface.getNewTransformStage().close();
      } catch (IllegalArgumentException e) {
        UserInterface.popUpError(e.getMessage());
      }
    });
    return save;
  }

  /**
   * Creates the button for saving the input for a new transformation. It will check what kind of
   * transformation the user has chosen, and then call the correct method for saving the input.
   *
   * @return The next button.
   */

  protected static Button nextButton(boolean isJulia, List<Double> valueList) {
    Button next = new Button("Next");
    next.getStyleClass().add("button");
    next.setOnAction(t -> {
      try {
        MakeNewTransformation.checkList(valueList);
        MakeNewTransformation.addValuesToList(valueList);
        MakeNewTransformation.clearAndMakeNewScene(isJulia);
      } catch (Exception e) {
        UserInterface.popUpError(e.getMessage());
      }
    });
    return next;
  }

  /**
   * Creates the button for canceling the input for a new transformation.
   *
   * @return The cancel button.
   */

  protected static Button cancel() {
    Button cancel = new Button("Cancel");
    cancel.getStyleClass().add("button");
    cancel.setOnAction(c -> UserInterface.getNewTransformStage().close());
    return cancel;
  }

  /**
   * Creates a {@code ChoiceBox} with {@code String} values where the user can choose which file to
   * read a description from. The method will also update the center pane with the chosen
   * description read from the chosen file.
   */

  public ChoiceBox<String> readFromFile() {
    ChoiceBox<String> choiceBox = MenuReadFromFile.createChoiceBox();
    choiceBox.setOnAction(e -> MainTab.setTopRow(MainTab.topRow()));
    try {
      choiceBox.getSelectionModel().selectedItemProperty()
          .addListener((observable, old, newValue) -> {
            if (MenuReadFromFile.getDescription() != null) {
              ChaosGameDescription description = MenuReadFromFile.getDescription();
              MainTab.updateCenterPane(CanvasView.createCanvas(description, 15000));
            }
          });
    } catch (IllegalArgumentException e) {
      UserInterface.popUpError(e.getMessage());
    }
    return choiceBox;
  }
}

