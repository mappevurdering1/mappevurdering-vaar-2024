package idatt2003.ntnu.mappe.gr37.button.menu;

import idatt2003.ntnu.mappe.gr37.App.UserInterface;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameFileHandler;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javafx.scene.control.ChoiceBox;

/**
 * This class is responsible for reading the description from a file and creating a
 * {@code ChoiceBox} with the descriptions that can be read from the file.
 * The class contains a method that reads the description from the file and creates a new
 * {@code ChaosGameDescription} object from the file.
 *
 * @see ChaosGameDescription
 * @see ChaosGameFileHandler
 */

public class MenuReadFromFile {
  private static ChoiceBox<String> files;
  private static ChaosGameDescription description;

  /**
   * Access method for the {@code ChaosGameDescription} read from the file.
   *
   * @return the {@code ChaosGameDescription} object.
   */

  protected static ChaosGameDescription getDescription() throws IllegalArgumentException {
    if (description == null) {
      throw new IllegalArgumentException("No file has been selected yet.");
    }
    return description;
  }

  /**
   * This method creates the {@code ChoiceBox} with {@code String} elements for the menu.
   * If the description.txt file has a description in it, it will let the user select that
   * description to read as one of the choices.
   *
   * @return the ChoiceBox with
   */

  protected static ChoiceBox<String> createChoiceBox() throws IllegalArgumentException {
    List<String> choices = new ArrayList<>();

    Collections.addAll(choices, "Your description", "Julia", "Sierpinski", "Barnsley");

    files = new ChoiceBox<>();
    files.setValue("Read from file");
    setChoices(choices);
    files.getSelectionModel().selectedItemProperty().addListener((observable, old, newValue) -> {
      String path;
      if (newValue.equals("Julia")) {
        path = "julia";
        description = readFromFileToCanvas(path);
      }
      if (newValue.equals("Sierpinski")) {
        path = "sierpinski";
        description = readFromFileToCanvas(path);
      }
      if (newValue.equals("Barnsley")) {
        path = "barnsley-fern";
        description = readFromFileToCanvas(path);
      }
      if (newValue.equals("Your description")) {
        path = "description";
        boolean fileHasText = checkDescriptionFile();
        if (fileHasText) {
          description = readFromFileToCanvas(path);
        } else {
          UserInterface.popUpError("You need to create your own transformation "
              + "to read it.");
        }
      }
    });
    return files;
  }

  /**
   * This method sets the elements to the {@code ChoiceBox} from a list with the {@code String}
   * elements.
   *
   * @param list a list with {@code String} values.
   */

  private static void setChoices(List<String> list) {
    list.forEach(s -> files.getItems().add(s));
  }

  /**
   * This method checks the description.txt file if it contains any text.
   * If the {@code Scanner} detects any text, the method returns true since the description.txt
   * file contains a description, and can therefore be added to the choice box to be read from.
   *
   * @return boolean value if the description.txt contains text or not.
   */

  public static boolean checkDescriptionFile() {
    String path = "src/main/resources/idatt2023.ntnu.mappe.gr37.files/description.txt";
    boolean hasText = false;
    try (Scanner reader = new Scanner(new FileReader(path))) {
      if (reader.hasNextLine()) {
        String line = reader.nextLine();
        if (!line.isBlank() || !line.isEmpty()) {
          hasText = true;
        }
      } else {
        reader.close();
      }
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
    return hasText;
  }

  /**
   * This method reads the description from the desired path by calling the {@code readFromFile}
   * method in the {@code ChaosGameFileHandler} class. And creates a new description from what it
   * reads.
   *
   * @param path the file name.
   * @return a new {@code ChaosGameDescription} result from reading the file.
   */

  private static ChaosGameDescription readFromFileToCanvas(String path) {
    ChaosGameDescription result = null;
    String fullPath = "src/main/resources/idatt2023.ntnu.mappe.gr37.files/" + path + ".txt";
    try {
      result = ChaosGameFileHandler.readFromFile(fullPath);
    } catch (IOException e) {
      UserInterface.popUpError(e.getMessage());
    }
    return result;
  }
}
