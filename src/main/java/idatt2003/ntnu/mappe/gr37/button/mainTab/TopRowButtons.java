package idatt2003.ntnu.mappe.gr37.button.mainTab;

import idatt2003.ntnu.mappe.gr37.App.MainTab;
import idatt2003.ntnu.mappe.gr37.App.UserInterface;
import idatt2003.ntnu.mappe.gr37.Factory.BarnsleyFernDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.Factory.JuliaDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.Factory.SierpinskiDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.button.menu.MakeNewTransformation;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.handle.VerifyInput;
import idatt2003.ntnu.mappe.gr37.model.AffineTransform2D;
import idatt2003.ntnu.mappe.gr37.model.JuliaTransform;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The TopRowButtons class is responsible for creating the top row of the main tab.
 * The top row contains the input box, the spinner for the number of steps, the run game button and
 * either of the input fields for creating a new transformation to add to the existing
 * transformations.
 *
 * @see Spinner
 * @see TextField
 * @see HBox
 * @see VBox
 */

public class TopRowButtons {

  /**
   * The list with the new Sierpinski values to be added in the game.
   */

  private static List<Double> sierpinskiValues = new ArrayList<>();

  /**
   * The list with the new Barnsley values to be added in the game.
   */

  private static List<Double> barnsleyValues = new ArrayList<>();

  /**
   * The list with the new Julia values to be added in the game.
   */

  private static List<Double> juliaValues = new ArrayList<>();

  /**
   * The list with the new minimum and maximum coordinates for the game.
   */

  private static List<Double> minMaxcoords = new ArrayList<>();

  /**
   * The spinner for the number of steps to run the game.
   */

  private static Spinner<Integer> noOfSteps = createSpinner();

  /**
   * The predefined Sierpinski description to be used in the game.
   */

  private static ChaosGameDescription sierpinski
      = new SierpinskiDescriptionFactory().createGameDescription();

  /**
   * The predefined Barnsley description to be used in the game.
   */

  private static ChaosGameDescription barnsley
      = new BarnsleyFernDescriptionFactory().createGameDescription();

  /**
   * The predefined Julia description to be used in the game.
   */

  private static ChaosGameDescription julia
      = new JuliaDescriptionFactory().createGameDescription();

  /**
   * The new description to be used in the game.
   */
  private static ChaosGameDescription newDescription;

  /**
   * This method creates the top row of the main tab.
   *
   * @param inputBox The input box to add to the top row.
   * @return The top row of the main tab.
   */

  public static HBox createTopRow(HBox inputBox) {
    HBox topRow = new HBox();
    topRow.getStyleClass().add("top-bottom-row");
    VBox spinnerBox = new VBox();
    Text steps = new Text("Steps");
    steps.getStyleClass().add("small-text");
    spinnerBox.getChildren().addAll(steps, createSpinner());
    topRow.getChildren().addAll(spinnerBox, inputBox, runGameButton());
    return topRow;
  }

  /**
   * This method creates a VBox with text fields for the different affine values.
   *
   * @param isSierpinski boolean value to check if the affine values are for Sierpinski or Barnsley.
   * @return VBox with all text fields and text for the transformation.
   */

  public static HBox createAffine(boolean isSierpinski) {
    Text matrix = new Text("Matrix");
    Text vector = new Text("Vector");
    matrix.getStyleClass().add("small-text");
    vector.getStyleClass().add("small-text");
    TextField a00 = new TextField();
    a00.setPromptText("a00");
    TextField a01 = new TextField();
    a01.setPromptText("a01");
    TextField a10 = new TextField();
    a10.setPromptText("a10");
    TextField a11 = new TextField();
    a11.setPromptText("a11");
    GridPane matrixGrid = new GridPane();
    matrixGrid.addRow(0, a00, a01);
    matrixGrid.addRow(1, a10, a11);

    TextField b0 = new TextField();
    b0.setPromptText("b0");
    TextField b1 = new TextField();
    b1.setPromptText("b1");
    VBox matrixContent = new VBox();
    VBox vectorBox = new VBox();
    vectorBox.getChildren().addAll(vector, b0, b1);
    matrixContent.getChildren().addAll(matrix, matrixGrid);

    List<TextField> fields = new ArrayList<>();
    Collections.addAll(fields, a00, a01, a10, a11, b0, b1);
    if (isSierpinski) {
      addListener(fields, sierpinskiValues);
    } else {
      addListener(fields, barnsleyValues);
    }
    VBox coordBox = createMinMaxCoord();
    HBox affine = new HBox();
    affine.getChildren().addAll(coordBox, matrixContent, vectorBox);
    return affine;
  }

  /**
   * This method creates a VBox with text fields for the different Julia values.
   *
   * @return VBox with all text fields and text.
   */

  public static HBox createJulia() {
    Text text = new Text("Julia values");
    text.getStyleClass().add("small-text");
    TextField real = new TextField();
    real.setPromptText("real number");
    TextField imag = new TextField();
    imag.setPromptText("imaginary number");
    List<TextField> fields = new ArrayList<>();
    Collections.addAll(fields, real, imag);
    HBox julia = new HBox();
    VBox coordBox = createMinMaxCoord();
    fields.forEach(t -> julia.getChildren().add(t));
    HBox box = new HBox();
    VBox juliaContent = new VBox();
    juliaContent.getChildren().addAll(text, julia);
    box.getChildren().addAll(coordBox, juliaContent);
    addListener(fields, juliaValues);
    return box;
  }

  /**
   * This method creates a VBox with text fields for the minimum and maximum coordinates.
   *
   * @return VBox with text fields for the minimum and maximum coordinates.
   */

  private static VBox createMinMaxCoord() {
    Text coordinates = new Text("Coordinates");
    coordinates.getStyleClass().add("small-text");
    TextField min0 = new TextField();
    TextField min1 = new TextField();
    TextField max0 = new TextField();
    TextField max1 = new TextField();
    VBox box = new VBox();
    GridPane grid = new GridPane();
    grid.addRow(0, min0, max0);
    grid.addRow(1, min1, max1);
    box.getChildren().addAll(coordinates, grid);

    List<TextField> coordValues = new ArrayList<>();
    Collections.addAll(coordValues, min0, min1, max0, max1);
    min0.setPromptText("x0");
    min1.setPromptText("x1");
    max0.setPromptText("y0");
    max1.setPromptText("y1");
    addListener(coordValues, minMaxcoords);
    return box;
  }

  /**
   * This method takes the values from the text fields and adds them to the desired list of values.
   * The method will check which list to add values to, and then clear it, add new values, and call
   * the {@code transferFieldsToList} method.
   *
   * @param fields List of {@code TextFields} to iterate through to add listener.
   * @param valueList the list which the values from the text field will be added to.
   * @throws IllegalArgumentException throws exception if the new value is not convertible to a
   *         double value, or if the index of the list being iterated is out of bounds.
   */

  private static void addListener(List<TextField> fields, List<Double> valueList)
      throws IllegalArgumentException {
    if (valueList == juliaValues) {
      juliaValues.clear();
      juliaValues = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 2) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, juliaValues);

    } else if (valueList == sierpinskiValues) {
      sierpinskiValues.clear();
      sierpinskiValues = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 6) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, sierpinskiValues);

    } else if (valueList == barnsleyValues) {
      barnsleyValues.clear();
      barnsleyValues = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 6) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, barnsleyValues);

    } else if (valueList == minMaxcoords) {
      minMaxcoords.clear();
      minMaxcoords = new ArrayList<>(Collections.nCopies(fields.size(), 0.0));
      if (fields.size() != 4) {
        throw new IllegalArgumentException("All inputs were not registered, please try again.");
      }
      transferFieldsToList(fields, minMaxcoords);
    }
  }

  /**
   * The method takes in a list of {@code TextFields} to add listeners to.
   * The text fields can only hold numbers and has a specific index for each value. Therefore,
   * each index is transferred to the new list that hold the {@code Double} new values from the
   * text fields.
   *
   * @param fields List of TextFields to iterate through to add listener.
   * @param valueList the list which the values from the text field will be added to.
   * @throws IllegalArgumentException throws exception if the new value is not convertible to a
   *          double value, or if the index of the list being iterated is out of bounds.
   */

  private static void transferFieldsToList(List<TextField> fields, List<Double> valueList) {
    for (TextField f : fields) {
      int index = fields.indexOf(f);
      f.textProperty().addListener((observable, old, newValue) -> {
        if (newValue.matches("-?\\d+(\\.\\d+)?")) {
          try {
            double value = VerifyInput.convertToDouble(newValue);
            valueList.set(index, value);
          } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
          }
        }
        System.out.println(valueList);
      });
    }
    try {
      setNewDescription(valueList);
    } catch (IllegalArgumentException e) {
      UserInterface.popUpError(e.getMessage());
    }
  }

  /**
   * This method takes in a list of {@code Double} values and checks which transformation it belongs
   * to. Then it will add the new inputs from the list to the existing transformation.
   *
   * @param values list of new {@code Double} values from input.
   */

  private static void setNewDescription(List<Double> values)
      throws IllegalArgumentException {
    if (values.equals(juliaValues)) {
      JuliaTransform newJulia =
          MakeNewTransformation.makeNewJulia(values.getFirst(), values.getLast());
      List<Transform2D> transforms = julia.getTransforms();
      transforms.add(newJulia);
      newDescription
        = new ChaosGameDescription(julia.getMinCoords(), julia.getMaxCoords(), transforms);

    } else if (values.equals(sierpinskiValues)) {
      Matrix2x2 m = new Matrix2x2(values.getFirst(), values.get(1), values.get(2), values.get(3));
      Vector2D b = new Vector2D(values.get(4), values.getLast());
      AffineTransform2D newSierpinski = MakeNewTransformation.makeNewAffine(m, b);
      List<Transform2D> transforms = sierpinski.getTransforms();
      transforms.add(newSierpinski);
      newDescription
        = new ChaosGameDescription(sierpinski.getMinCoords(), sierpinski.getMaxCoords(),
        transforms);

    } else if (values.equals(barnsleyValues)) {
      Matrix2x2 m = new Matrix2x2(values.getFirst(), values.get(1), values.get(2), values.get(3));
      Vector2D b = new Vector2D(values.get(4), values.getLast());
      AffineTransform2D newBarnsley = MakeNewTransformation.makeNewAffine(m, b);
      List<Transform2D> transforms = barnsley.getTransforms();
      transforms.add(newBarnsley);
      newDescription
        = new ChaosGameDescription(barnsley.getMinCoords(), barnsley.getMaxCoords(), transforms);
    }
  }

  /**
   * Accessor method for the new description.
   *
   * @return the new {@code ChaosGameDescription}.
   */

  public static ChaosGameDescription getNewDescription() {
    return newDescription;
  }

  /**
   * This method creates a spinner for the number of steps to run the Game.
   *
   * @return {@code Spinner} with integer values.
   */

  public static Spinner<Integer> createSpinner() {
    noOfSteps = new Spinner<>(1, 10000000, 1);
    noOfSteps.setPromptText("steps");
    noOfSteps.setEditable(true);
    noOfSteps.setMinWidth(100);
    String inputValue = String.valueOf(noOfSteps.getValueFactory().getValue());
    try {
      int spinnerValue = VerifyInput.checkInteger(inputValue);
      noOfSteps.getValueFactory().setValue(spinnerValue);
    } catch (IllegalArgumentException e) {
      UserInterface.popUpError(e.getMessage());
    }
    return noOfSteps;
  }

  /**
   * The runGameButton() method runs the game with the new transformation values added to the
   * predefined {@code ChaosGameDescriptions} description.
   *
   * @return the run game button.
   */

  private static Button runGameButton() {
    Button run = new Button("Run game");
    run.getStyleClass().add("run-button");
    run.setPrefSize(100, 70);
    run.setOnAction(r -> {
      int steps = noOfSteps.getValue();
      if (newDescription != null) {
        if (steps >= 10) {
          MainTab.updateCenterPane(CanvasView.createCanvas(getNewDescription(), steps));
        } else {
          UserInterface.popUpError("You cannot run the game with steps < 9");
        }
      } else {
        UserInterface.popUpError("Please add your own transformation values"
            + " before you run the game.");
      }
    });
    return run;
  }
}