package idatt2003.ntnu.mappe.gr37.Factory;

import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;

/**
 * The ChaosGameDescriptionFactory interface is responsible for creating a new game description.
 *
 * @see ChaosGameDescription
 */

public interface ChaosGameDescriptionFactory {
  /**
   * Creates a new game description.
   *
   * @return The new game description.
   */
  ChaosGameDescription createGameDescription();
}
