package idatt2003.ntnu.mappe.gr37.Factory;

import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.model.AffineTransform2D;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The BarnsleyFernDescriptionFactory class implements the ChaosGameDescriptionFactory interface,
 * and is responsible for creating a new game description for the barnsley fern transformation.
 *
 * @see ChaosGameDescriptionFactory
 * @see ChaosGameDescription
 */

public class BarnsleyFernDescriptionFactory implements ChaosGameDescriptionFactory {

  /**
   * The method creates a new game description of the barnsley fern transformation.
   *
   * @return the {@code ChaosGameDescription} object.
   */

  @Override
  public ChaosGameDescription createGameDescription() {
    ChaosGameDescription description;
    List<Transform2D> transforms = new ArrayList<>();
    try {
      AffineTransform2D barnsley1 = new AffineTransform2D(new Matrix2x2(0.0, 0.0,
          0.0, 0.16), new Vector2D(0.0, 0.0));
      AffineTransform2D barnsley2 = new AffineTransform2D(new Matrix2x2(0.85, 0.04,
          -0.04, 0.85), new Vector2D(0.0, 1.6));
      AffineTransform2D barnsley3 = new AffineTransform2D(new Matrix2x2(0.2, -0.26,
          0.23, 0.22), new Vector2D(0.0, 1.6));
      AffineTransform2D barnsley4 = new AffineTransform2D(new Matrix2x2(-0.15, 0.28,
          0.26, 0.24), new Vector2D(0.0, 0.44));
      Collections.addAll(transforms, barnsley1, barnsley2, barnsley3, barnsley4);
      description = new ChaosGameDescription(new Vector2D(-2.65, 0.0),
          new Vector2D(2.65, 10.0), transforms);
    } catch (Exception e) {
      throw new IllegalArgumentException(e.getMessage());
    }
    return description;
  }
}
