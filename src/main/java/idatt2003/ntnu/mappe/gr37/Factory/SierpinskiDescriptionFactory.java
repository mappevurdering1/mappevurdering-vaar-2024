package idatt2003.ntnu.mappe.gr37.Factory;

import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.model.AffineTransform2D;
import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The SierpinskiDescriptionFactory class is responsible for creating a new Sierpinski game
 * description. The class implements the ChaosGameDescriptionFactory interface.
 *
 * @see ChaosGameDescriptionFactory
 * @see ChaosGameDescription
 */

public class SierpinskiDescriptionFactory implements ChaosGameDescriptionFactory {

  /**
   * Constructor for the SierpinskiDescriptionFactory class.
   *
   * @return the {@code ChaosGameDescription} object.
   */

  @Override
  public ChaosGameDescription createGameDescription() {
    ChaosGameDescription description;
    List<Transform2D> transforms = new ArrayList<>();
    try {
      AffineTransform2D sierpinski1 = new AffineTransform2D(new Matrix2x2(0.5, 0.0,
          0.0, 0.5), new Vector2D(0.0, 0.0));
      AffineTransform2D sierpinski2 = new AffineTransform2D(new Matrix2x2(0.5, 0.0,
          0.0, 0.5), new Vector2D(0.25, 0.5));
      AffineTransform2D sierpinski3 = new AffineTransform2D(new Matrix2x2(0.5, 0.0,
          0.0, 0.5), new Vector2D(0.5, 0.0));
      Collections.addAll(transforms, sierpinski1, sierpinski2, sierpinski3);
      description = new ChaosGameDescription(new Vector2D(0.0, 0.0), new Vector2D(1.0, 1.0),
        transforms);
    } catch (Exception e) {
      throw new IllegalArgumentException(e.getMessage());
    }
    return description;
  }
}
