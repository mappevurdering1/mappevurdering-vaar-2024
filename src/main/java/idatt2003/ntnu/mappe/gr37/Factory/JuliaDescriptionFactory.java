package idatt2003.ntnu.mappe.gr37.Factory;

import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.model.Complex;
import idatt2003.ntnu.mappe.gr37.model.JuliaTransform;
import idatt2003.ntnu.mappe.gr37.model.Transform2D;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The JuliaDescriptionFactory class is responsible for creating a new Julia set game description.
 * The class implements the ChaosGameDescriptionFactory interface.
 *
 * @see ChaosGameDescriptionFactory
 * @see ChaosGameDescription
 */

public class JuliaDescriptionFactory implements ChaosGameDescriptionFactory {

  /**
   * Constructor for the JuliaDescriptionFactory class.
   */

  public JuliaDescriptionFactory() {
  }

  /**
   * Creates a new game description of a Julia set.
   *
   * @return the {@code ChaosGameDescription} object.
   */

  @Override
  public ChaosGameDescription createGameDescription() {
    ChaosGameDescription description;
    List<Transform2D> transforms = new ArrayList<>();
    try {
      JuliaTransform transform1 = new JuliaTransform(new Complex(-0.74543, 0.11301),
          -1);
      JuliaTransform transform2 = new JuliaTransform(new Complex(-0.74543, 0.11301),
          1);
      Collections.addAll(transforms, transform1, transform2);
      description = new ChaosGameDescription(new Vector2D(-1.6, -1.0), new Vector2D(1.6, 1.0),
          transforms);
    } catch (Exception e) {
      throw new IllegalArgumentException(e.getMessage());
    }
    return description;
  }
}