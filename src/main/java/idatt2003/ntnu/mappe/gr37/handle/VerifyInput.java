package idatt2003.ntnu.mappe.gr37.handle;

/**
 * This class is a verification class with methods to verify the parameters used in the calculations
 * of the other classes. The class has method for verifying that the input is possible to convert
 * to either doubles or integers and that the values are not NaN.
 */

public class VerifyInput {

  /**
   * This method verifies that the input is possible to convert to a {@code Double} value.
   *
   * @param str {@code String} value to be converted to a {@code Double} value.
   * @return newDouble {@code Double} value that has been verified.
   * @throws IllegalArgumentException if it cant be converted to a {@code Double} value.
   */

  public static double convertToDouble(String str) throws IllegalArgumentException {
    double newDouble;
    try {
      newDouble = Double.parseDouble(str);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Invalid input: " + str + ". Expected a number.");
    }
    return newDouble;
  }

  /**
   * This method verifies that the  value is not of NaN type.
   *
   * @param number {@code Double} value to verify if it is NaN.
   * @throws IllegalArgumentException if it cant be converted to a
   *     Double value or if the value is NaN.
   */

  public static void verifyInputForNaN(Double number) throws IllegalArgumentException {
    if (number.isNaN() || number.isInfinite()) {
      throw new IllegalArgumentException("Input contains NaN values");
    }
  }

  /**
   * This method verifies that the input is possible to convert to an {@code Integer} value.
   *
   * @param input {@code String} value to be converted to an {@code Integer} value
   * @return newInt {@code Integer} value that has been verified.
   */

  public static int checkInteger(String input) {
    int newInt;
    try {
      newInt = Integer.parseInt(input);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Invalid input: " + input
          + ". Expected an Integer number");
    }
    return newInt;
  }

}
