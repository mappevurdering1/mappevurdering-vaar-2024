package idatt2003.ntnu.mappe.gr37;

import idatt2003.ntnu.mappe.gr37.Factory.BarnsleyFernDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.Factory.JuliaDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.Factory.SierpinskiDescriptionFactory;
import idatt2003.ntnu.mappe.gr37.game.ChaosGame;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameDescription;
import idatt2003.ntnu.mappe.gr37.game.ChaosGameFileHandler;
import idatt2003.ntnu.mappe.gr37.handle.VerifyInput;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This Main class is used to run the system from the console. The developers used it under
 * development to test the different methods and calculations and check the result of the
 * fractals with a result displayed in the console.
 */

public class Main {

  /**
   * The main method for the application. It is used to run the system from the console.
   * It has a menu where the user can choose between reading from a file, writing to a file,
   * printing an ASCII fractal, or exiting the program.
   *
   * @param args The command line arguments.
   * @throws IOException If an I/O error occurs.
   */

  public static void main(String[] args) throws IOException {
    try (Scanner scanner = new Scanner(System.in)) {

      boolean running = true;

      while (running) {
        System.out.println("Chaos Game!");
        System.out.println("""
            Choose a game mode:\s
            1. Read from file\s
            2. Write description to file\s
            3. Print ASCII fractal\s
            4. Exit""");

        while (!scanner.hasNextInt()) {
          System.out.println("Invalid input. Please try again.");
          scanner.next();
        }
        int choice = scanner.nextInt();
        VerifyInput.checkInteger(String.valueOf(choice));
        switch (choice) {
          case 1 -> {
            System.out.println("""
                Chose file to read from:\s
                1. julia.txt\s
                2. sierpinski.txt\s
                3. barnsley-fern.txt\s
                """);
            while (!scanner.hasNextInt()) {
              System.out.println("Invalid input. Please try again.");
              scanner.next();
            }
            int fileChoice = scanner.nextInt();
            VerifyInput.checkInteger(String.valueOf(fileChoice));
            switch (fileChoice) {
              case 1 -> new JuliaDescriptionFactory().createGameDescription();
              case 2 -> new SierpinskiDescriptionFactory().createGameDescription();
              case 3 -> new BarnsleyFernDescriptionFactory().createGameDescription();
              default -> System.out.println("Invalid input. Please try again.");
            }
          }
          case 2 -> {
            try {
              String writeToThisPath =
                  "src/main/resources/idatt2023.ntnu.mappe.gr37.files/description.txt";
              String resourcePath =
                  "src/main/resources/idatt2023.ntnu.mappe.gr37.files/sierpinski.txt";
              ChaosGameDescription description = ChaosGameFileHandler.readFromFile(resourcePath);
              ChaosGameFileHandler.writeToFile(description, writeToThisPath);
            } catch (IOException e) {
              System.out.println(e.getMessage());
            }
          }
          case 3 -> {
            int fractalChoice = 0;
            boolean validChoice = false;
            while (!validChoice) {
              System.out.println("""
                Chose fractal to draw:\s
                1. julia.txt\s
                2. sierpinski.txt\s
                3. barnsley-fern.txt\s
                 \s""");
              while (!scanner.hasNextInt()) {
                System.out.println("Invalid input. Please try again.");
                scanner.next();
              }
              fractalChoice = scanner.nextInt();
              VerifyInput.checkInteger(String.valueOf(fractalChoice));
              switch (fractalChoice) {
                case 1, 2, 3 -> validChoice = true;
                default -> System.out.println("Invalid choice. Please try again.");
              }
            }
            String path = switch (fractalChoice) {
              case 1 -> "julia";
              case 2 -> "sierpinski";
              case 3 -> "barnsley-fern";
              default -> throw new IllegalStateException("Unexpected value: " + fractalChoice);
            };
            System.out.println("Enter the number of steps to run:");
            while (!scanner.hasNextInt()) {
              System.out.println("Invalid input. Please try again.");
              scanner.next();
            }
            int steps = scanner.nextInt();
            VerifyInput.checkInteger(String.valueOf(steps));
            ChaosGameDescription description = ChaosGameFileHandler.readFromFile(
                "src/main/resources/idatt2023.ntnu.mappe.gr37.files/" + path + ".txt");
            runGame(description, steps);
          }
          case 4 -> {
            System.out.println("Exiting...");
            running = false;
          }
          default -> System.out.println("Invalid input. Please try again.");
        }
      }
    } catch (InputMismatchException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Run the game with a ChaosGameDescription and an amount of steps.
   * The game is run with the description and the amount of steps given.
   * The canvas is printed to the console.
   *
   * @param description The min and max coordinates of the canvas, and a list of transformations.
   *                    As a ChaosGameDescription object.
   * @param steps The amount of steps to run the game.
   */

  public static ChaosGame runGame(ChaosGameDescription description, int steps) {
    int width = 50;
    int height = 50;
    ChaosGame game = new ChaosGame(description, width, height);
    game.runSteps(steps);
    StringBuilder printCanvas = new StringBuilder();
    for (int[] row : game.getCanvas().getCanvasArray()) {
      for (int col : row) {
        if (col > 0) {
          printCanvas.append("X").append(" ");
        } else {
          printCanvas.append("-").append(" ");
        }
      }
      printCanvas.append("\n");
    }
    System.out.println(printCanvas);
    return game;
  }
}