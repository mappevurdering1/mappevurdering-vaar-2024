package idatt2003.ntnu.mappe.gr37.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AffineTransform2DTest {
  @Nested
  class AffineTransform2DPositiveTest {

    @Test
    @DisplayName("Test that transform transforms a 2D vector")
    void testTransform() {
      // Arrange
      Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
      Vector2D vector = new Vector2D(5.0, 6.0);
      AffineTransform2D affineTransform2D = new AffineTransform2D(matrix, vector);
      // Act
      Vector2D inputVector = new Vector2D(7.0, 8.0);
      Vector2D result = affineTransform2D.transform(inputVector);
      // Assert
      assertNotNull(result);
    }

    @Test
    @DisplayName("Test that getMatrix returns the matrix of the Affine Transformation")
    void testGetMatrix() {
      // Arrange
      Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
      Vector2D vector = new Vector2D(5.0, 6.0);
      AffineTransform2D affineTransform2D = new AffineTransform2D(matrix, vector);
      // Act
      Matrix2x2 result = affineTransform2D.getMatrix();
      // Assert
      assertEquals(matrix, result);
    }

    @Test
    @DisplayName("Test that getVector returns the vector of the Affine Transformation")
    void testGetVector() {
      // Arrange
      Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
      Vector2D vector = new Vector2D(5.0, 6.0);
      AffineTransform2D affineTransform2D = new AffineTransform2D(matrix, vector);
      // Act
      Vector2D result = affineTransform2D.getVector();
      // Assert
      assertEquals(vector, result);
    }
  }

  @Nested
  class AffineTransform2DNegativeTest {
    @Test
    @DisplayName("Test that transform with null vector throws NullPointerException")
    void testTransformWithNullVector() {
      // Arrange
      Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
      Vector2D vector = new Vector2D(5.0, 6.0);
      AffineTransform2D affineTransform2D = new AffineTransform2D(matrix, vector);
      Vector2D inputVector = null;
      // Act and Assert
      assertThrows(NullPointerException.class, () -> affineTransform2D.transform(inputVector));
    }

    @Test
    @DisplayName("test with invalid matrix")
    void testInvalidMatrix() {
      // Arrange
      Matrix2x2 matrix = null;
      Vector2D vector = new Vector2D(5.0, 6.0);
      // Act and Assert
      assertDoesNotThrow(() -> new AffineTransform2D(matrix, vector));
    }

    @Test
    @DisplayName("test with invalid vector")
    void testInvalidVector() {
      // Arrange
      Matrix2x2 matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
      Vector2D vector = null;
      // Act and Assert
      assertDoesNotThrow(() -> new AffineTransform2D(matrix, vector));
    }

  }
}
