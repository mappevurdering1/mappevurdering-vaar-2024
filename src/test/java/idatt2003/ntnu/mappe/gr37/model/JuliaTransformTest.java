package idatt2003.ntnu.mappe.gr37.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class JuliaTransformTest {

  @Nested
  class JuliaTransformPositiveTest {

    @Test
    @DisplayName("Test that transform transforms a 2D vector")
    void testTransform() {
      // Arrange
      Complex point = new Complex(1.0, 2.0);
      JuliaTransform juliaTransform = new JuliaTransform(point, 1);
      Vector2D vector = new Vector2D(3.0, 4.0);
      // Act
      Vector2D transformResult = juliaTransform.transform(vector);
      // Assert
      assertTrue(transformResult instanceof Vector2D);
      assertNotNull(transformResult);
    }

    @Test
    @DisplayName("Test that getSign returns the sign of the imaginary part of the complex number")
    void testGetPoint() {
      Complex point = new Complex(1.0, 2.0);
      JuliaTransform juliaTransform = new JuliaTransform(point, 1);
      assertEquals(point, juliaTransform.getPoint());
    }

    @Test
    void testGetSign() {
      Complex point = new Complex(1.0, 2.0);
      JuliaTransform juliaTransform = new JuliaTransform(point, 1);
      assertEquals(1, juliaTransform.getSign());
    }
  }

  @Nested
  class JuliaTransformNegativeTest {
    @Test
    @DisplayName("Test that ytansform with null vector throws NullPointerException")
    void testTransformWithNullVector() {
      // Arrange
      Complex point = new Complex(1.0, 2.0);
      JuliaTransform juliaTransform = new JuliaTransform(point, 1);
      Vector2D vector = null;
      // Act and Assert
      assertThrows(NullPointerException.class, () -> juliaTransform.transform(vector));
    }

    @Test
    @DisplayName("Test with invalid sign")
    void testInvalidSign() {
      // Arrange
      Complex point = new Complex(1.0, 2.0);
      // Act and Assert
      assertDoesNotThrow(() -> new JuliaTransform(point, 0));
    }
  }
}
