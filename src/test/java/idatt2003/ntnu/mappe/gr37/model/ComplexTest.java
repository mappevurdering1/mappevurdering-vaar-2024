package idatt2003.ntnu.mappe.gr37.model;

import idatt2003.ntnu.mappe.gr37.model.Complex;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;


public class ComplexTest {

    @Nested
    class ComplexPositiveTest {

        @Test
        @DisplayName("Test that add adds two complex numbers together")
        void add() {
            // Arrange
            Complex complex1 = new Complex(1.0, 2.0);
            Complex complex2 = new Complex(3.0, 4.0);
            // Act
            Vector2D addResult = complex1.add(complex2);
            // Assert
            assertTrue(addResult instanceof Vector2D);
            assertEquals(4.0, addResult.getX0());
            assertEquals(6.0, addResult.getX1());
        }

        @Test
        @DisplayName("Test that add adds two complex numbers together, negative numbers")
        void add2() {
            // Arrange
            Complex complex1 = new Complex(-1.0, -2.0);
            Complex complex2 = new Complex(-3.0, -4.0);
            // Act
            Vector2D addResult = complex1.add(complex2);
            // Assert
            assertTrue(addResult instanceof Vector2D);
            assertEquals(-4.0, addResult.getX0());
            assertEquals(-6.0, addResult.getX1());
        }

        @Test
        @DisplayName("Test that subtract subtracts two complex numbers from each other")
        void subtract() {
            // Arrange
            Complex complex1 = new Complex(1.0, 4.0);
            Complex complex2 = new Complex(3.0, 2.0);
            // Act
            Vector2D subtractResult = complex1.subtract(complex2);
            // Assert
            assertTrue(subtractResult instanceof Vector2D);
            assertEquals(-2.0, subtractResult.getX0());
            assertEquals(2.0, subtractResult.getX1());
        }

        @Test
        @DisplayName("Test that subtract subtracts two complex numbers from each other")
        void subtract2() {
            // Arrange
            Complex complex1 = new Complex(-1.0, -4.0);
            Complex complex2 = new Complex(-3.0, -2.0);
            // Act
            Vector2D subtractResult = complex1.subtract(complex2);
            // Assert
            assertTrue(subtractResult instanceof Vector2D);
            assertEquals(2.0, subtractResult.getX0());
            assertEquals(-2.0, subtractResult.getX1());
        }

        @Test
        @DisplayName("Test that dot returns the dot product of two complex numbers")
        void dot() {
            // Arrange
            Complex complex1 = new Complex(1.0, 2.0);
            Complex complex2 = new Complex(3.0, 4.0);
            // Act
            double dotResult = complex1.dot(complex2);
            // Assert
            assertEquals(11, dotResult);
        }

        @Test
        @DisplayName("Test that dot returns the dot product of two complex negative numbers")
        void dot2() {
            // Arrange
            Complex complex1 = new Complex(-1.0, -2.0);
            Complex complex2 = new Complex(-3.0, -4.0);
            // Act
            double dotResult = complex1.dot(complex2);
            // Assert
            assertEquals(11, dotResult);
        }

        @Test
        @DisplayName("Test that scalar multiplies a complex number with a scalar")
        void scalar() {
            // Arrange
            Complex complex = new Complex(1.0, 2.0);
            Double scalar = 2.0;
            // Act
            Vector2D scalarResult = complex.scalar(scalar);
            // Assert
            assertEquals(2.0, scalarResult.getX0());
            assertEquals(4.0, scalarResult.getX1());
        }

        @Test
        @DisplayName("Test that scalar multiplies a complex number with a scalar, negative numbers")
        void scalar2() {
            // Arrange
            Complex complex = new Complex(1.0, 2.0);
            Double scalar = -2.0;
            // Act
            Vector2D scalarResult = complex.scalar(scalar);
            // Assert
            assertTrue(scalarResult instanceof Vector2D);
            assertEquals(-2.0, scalarResult.getX0());
            assertEquals(-4.0, scalarResult.getX1());
        }

        @Test
        @DisplayName("Test that sqrt returns the square root of a complex number")
        void sqrt() {
            // Arrange
            Complex complex = new Complex(2.0, 2.0);
            // Act
            Vector2D sqrtResult = complex.sqrt();
            // Assert
            assertTrue(sqrtResult instanceof Vector2D);
            assertEquals(1.5538, Math.round(sqrtResult.getX0()), 4);
            assertEquals(0.6436, Math.round(sqrtResult.getX1()), 4);
        }
    }

    @Nested
    class ComplexNegativeTest {
        @Test
        @DisplayName("Failed test for Complex constructor, IllegalArgumentException.")
        void constructorFail() {
            NullPointerException trown = assertThrows(NullPointerException.class, () -> {
                Complex complex = new Complex((Double) null, 2.0);
            });

        }

        @Test
        @DisplayName("Failed test for add method, IllegalArgumentException.")
        void addFail() {
            NullPointerException trown = assertThrows(NullPointerException.class, () -> {
                Complex complex1 = new Complex((Double) null, 2.0);
                Complex complex2 = new Complex(3.0, 4.0);
                Vector2D addResult = complex1.add(complex2);
            });
        }


        @Test
        @DisplayName("Failed test for subtract method, nullpointer exception.")
        void subtractFail() {
            NullPointerException trown = assertThrows(NullPointerException.class, () -> {
                Complex complex1 = new Complex(1.0, 2.0);
                Complex complex2 = new Complex((Double) null, 4.0);
                Vector2D subtractResult = complex1.subtract(complex2);
            });
        }

        @Test
        @DisplayName("Failed test for dot method, IllegalArgumentException.")
        void dotFail() {
            NullPointerException trown = assertThrows(NullPointerException.class, () -> {
                Complex complex1 = new Complex(1.0, 2.0);
                Complex complex2 = new Complex(3.0, (Double) null);
                double dotResult = complex1.dot(complex2);
            });
        }

        @Test
        @DisplayName("Failed test for scalar method, nullpointer exception.")
        void scalarFail() {
            NullPointerException trown = assertThrows(NullPointerException.class, () -> {
                Complex complex = new Complex(1.0, 2.0);
                Double scalar = null;
                Vector2D scalarResult = complex.scalar(scalar);
            });
        }

        @Test
        @DisplayName("Failed test for sqrt method with null values, NullPointerException.")
        void sqrtNullFail() {
            assertThrows(NullPointerException.class, () -> {
                Complex complex = new Complex((Double) null, 2.0).sqrt();
            });
        }

        @Test
        @DisplayName("Failed test for divide method with null values, nullpointer exception.")
        void divideNullFail() {
            Complex complex = new Complex(2.0, 2.0);
            assertThrows(NullPointerException.class, () -> {
                complex.divide((Double) null);
            });
        }

        @Test
        @DisplayName("Failed test for divide method with zero divisor, arithmetic exception.")
        void divideZeroFail() {
            Complex complex = new Complex(2.0, 2.0);
            assertThrows(ArithmeticException.class, () -> {
                complex.divide(0.0);
            });
        }

        @Test
        @DisplayName("Failed test for multiply method with null values, nullpointer exception.")
        void multiplyNullFail() {
            Complex complex = new Complex(2.0, 2.0);
            assertThrows(NullPointerException.class, () -> {
                complex.multiply((Double) null);
            });
        }

        @Test
        @DisplayName("Failed test for sqrt method with NaN values, NumberFormatException.")
        void sqrtNaNFail() {
            double a = Double.NaN;
            double b = 0.0;
            assertThrows(NumberFormatException.class, () -> {
                Complex complex = new Complex(a, b).sqrt();
            });
        }
    }
}




