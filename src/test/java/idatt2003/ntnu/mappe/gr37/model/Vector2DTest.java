package idatt2003.ntnu.mappe.gr37.model;
import static org.junit.jupiter.api.Assertions.*;

import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class Vector2DTest {

  @Nested
  @DisplayName("Positive tests for all methods")
  class PositiveTests {
    static Vector2D v1;
    static Vector2D v2;
    static Vector2D v3;

    @BeforeAll
    @DisplayName("Set up vectors, test constructor")
    static void init() {
      v1 = new Vector2D(1.0, 2.0);
      v2 = new Vector2D(10.0, 14.0);
      v3 = new Vector2D(2.0, 2.0);
      assertEquals(v1.getX0(), 1.0);
      assertEquals(v1.getX1(), 2.0);
      assertEquals(v2.getX0(), 10.0);
      assertEquals(v2.getX1(), 14.0);
      assertEquals(v3.getX0(), 2.0);
      assertEquals(v3.getX1(), 2.0);
    }

    @Test
    @DisplayName("Test add method")
    void testAdd() {
      Vector2D result = v1.add(v2);
      //assert
      assertEquals(11.0, result.getX0());
      assertEquals(16.0, result.getX1());
    }
    @Test
    @DisplayName("Test subtract method big - smaller")
    void testSubtract() {
      Vector2D result = v2.subtract(v3);
      //assert
      assertEquals(8.0, result.getX0());
      assertEquals(12.0, result.getX1());
    }
    @Test
    @DisplayName("Test subtract method smaller - big")
    void testSubtract2() {
      Vector2D result = v1.subtract(v2);
      //assert
      assertEquals(-9.0, result.getX0());
      assertEquals(-12.0, result.getX1());
    }
    @Test
    @DisplayName("Test dot method")
    void testDot() {
      double result = v3.dot(v1);
      //assert
      assertEquals(6.0, result);
    }
    @Test
    @DisplayName("Test scalar method")
    void testScalar() {
      Vector2D result = v3.scalar(5.0);
      //assert
      assertEquals(10.0, result.getX0());
    }
  }


  @Nested
  @DisplayName("Negative tests with null values")
  class NegativeTests {
    static Vector2D v1;
    static Vector2D v2;

    @BeforeAll
    @DisplayName("Set up vectors with null values, test constructor")
    static void setUp() {
      assertThrows(NullPointerException.class, () -> {
        v1 = new Vector2D((Double) null, 1.0);
        v2 = new Vector2D(10.0, 14.0);
      });
    }

    @Test
    @DisplayName("Add with null value")
    void addWithNullValue() {
      assertThrows(NullPointerException.class, () -> {
          Vector2D result = v1.add(v2);
        }
      );
    }

    @Test
    @DisplayName("Subtract with null value")
    void subtractWithNullValue() {
      assertThrows(NullPointerException.class, () -> {
          Vector2D v3 = v1.subtract(v2);
        }
      );
    }

    @Test
    @DisplayName("Dot with null value")
    void dotWithNullValue() {
      assertThrows(NullPointerException.class, () -> {
          double result = v1.dot(v2);
        }
      );
    }

    @Test
    @DisplayName("Scalar with null value")
    void scalarWithNullValue() {
      assertThrows(NullPointerException.class, () -> {
          Vector2D result = v1.scalar(2.0);
        }
      );
    }

    @Test
    @DisplayName("Set up vector with non-numeric value")
    void shouldThrowIllArgExcWhenValueIsInvalid() {
      assertThrows(IllegalArgumentException.class, () -> {
        Vector2D v = new Vector2D(Double.NaN, 2.0);
      });
    }
  }
}