package idatt2003.ntnu.mappe.gr37.model;

import static org.junit.jupiter.api.Assertions.*;

import idatt2003.ntnu.mappe.gr37.model.Matrix2x2;
import idatt2003.ntnu.mappe.gr37.model.Vector2D;
import org.junit.jupiter.api.*;

public class Matrix2x2Test {

  @Nested
  class positiveTests {
    static Matrix2x2 matrix;
    static Vector2D vector;
    @BeforeAll
    @DisplayName("Sets up the matrix")
    static void setUp() {
       matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
       vector = new Vector2D(1.0, 3.0);
    }
    @AfterEach
    @DisplayName("Tears down the matrix")
    void resetMatrix() {
      matrix = new Matrix2x2(1.0, 2.0, 3.0, 4.0);
    }

    @Test
    @DisplayName("Multiply positive matrix and vector")
    void shouldReturnNewVectorWhenMatrixMultiplied() {
      Vector2D result = matrix.multiply(vector);
      assertEquals(7.0, result.getX0());
      assertEquals(15.0, result.getX1());
    }
    @Test
    @DisplayName("Multiply matrix with negative vector")
    void shouldReturnNewVectorWithNegativeVectorMultiplication() {
      Vector2D v = new Vector2D(-1.0, -2.0);
      Vector2D result = matrix.multiply(v);
      assertEquals(-5.0, result.getX0());
      assertEquals(-11.0, result.getX1());
    }

    @Test
    @DisplayName("Multiply vector with negative matrix")
    void shouldReturnNewVectorWithNegativeMatrixValues() {
      Matrix2x2 m = new Matrix2x2(-1.0, -2.0, -3.0, -4.0);
      Vector2D result = m.multiply(vector);
      assertEquals(-7.0, result.getX0());
      assertEquals(-15.0, result.getX1());
    }

    @Test
    @DisplayName("Multiplication with 0-vector and matrix")
    void shouldReturnNew0Vector() {
      Vector2D v = new Vector2D(0.0, 0.0);
      Vector2D result = matrix.multiply(v);
      assertEquals(0.0, result.getX0());
      assertEquals(0.0, result.getX1());
    }

    @Test
    @DisplayName("Multiplication with matrix and 0-value in vector")
    void shouldReturnNewVectorWhenVectorHasA0() {
      Vector2D v = new Vector2D(0.0, 2.0);
      Vector2D result = matrix.multiply(v);
      assertEquals(4.0, result.getX0());
      assertEquals(8.0, result.getX1());
    }

    @Test
    @DisplayName("Multiplication where matrix has a 0-column")
    void shouldReturnNewVectorWhenMatrixHas0Column() {
      Matrix2x2 m = new Matrix2x2(0.0, 2.0, 0.0, 3.0);
      Vector2D result = m.multiply(vector);
      assertEquals(6.0, result.getX0());
      assertEquals(9.0, result.getX1());
    }
    @Test
    @DisplayName("Multiplication with full 0-matrix and vector")
    void shouldReturn0VectorWhenMatrixIs0() {
      Matrix2x2 m = new Matrix2x2(0.0, 0.0, 0.0, 0.0);
      Vector2D result = m.multiply(vector);
      assertEquals(0.0, result.getX0());
      assertEquals(0.0, result.getX1());
    }

  }

  @Nested
  class negativeTests {
    @Test
    @DisplayName("Test constructor with null value")
    void ShouldThrowIllArgExcFromConstructorWithNullValue() {
      assertThrows(NullPointerException.class, () -> {
        Matrix2x2 matrix = new Matrix2x2(1.0, (Double) null, 3.0, 4.0);
      });
    }

    @Test
    @DisplayName("Test constructor with non-numeric input")
    void shouldThrowNumFormExcWithNonNumericInput() {
      assertThrows(NumberFormatException.class, () -> {
        Matrix2x2 matrix = new Matrix2x2(1.0, Double.NaN, 3.0, 4.0);
      });
    }

    @Test
    @DisplayName("Multiply with non-numeric vector value")
    void shouldThrowIllArgExcWhenValueIsInvalid() {
      assertThrows(IllegalArgumentException.class, () -> {
      Vector2D v = new Vector2D(Double.NaN, 2.0);
      Matrix2x2 m = new Matrix2x2(2.0, 3.0, 4.0, 5.0);
      Vector2D result = m.multiply(v);
    });
    }

  }

}
