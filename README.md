# Mappevurdering vaar 2024

## Name
ChaosGame

## Description
ChaosGame is an application focused on generating digital fractals. The results are visually displayed in a graphical user interface (GUI). In this app, you can generate various fractals including the Sierpinski Triangle, Barnsley Fern, and Julia Set. Additionally, you can create your own custom fractals, edit existing ones, and save the images.

## Features
 - **generate fractals:** Sierpinski Triangle, Barnsley Fern, Julia Set
 - **Custom Fractals:** Create your own Julia or Affine fractals
 - **Edit Existing Fractals:** Edit existing fractals by changing the fractal's parameters
 - **Save Images:** Save the generated fractals as images

## Clone
git clone https://gitlab.stud.idi.ntnu.no/mappevurdering1/mappevurdering-vaar-2024

## Usage
To run the application, you need to have Java installed on your computer.
The application can be run by executing the `main` method in the `GameApp` class.
Or by using the following command in the terminal: 
>mvn javafx:run

## Authors and acknowledgment
 - **Ingrid Midtmoen Døvre**
 - **Anne Cecilie Skår Nilsen**

Acknowledgment to the course staff for the help and guidance throughout the project.